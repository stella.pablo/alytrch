<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $table ='cursos';

    protected  $fillable = ['titulo','descripcion','fecha','cupo','imagen','termino'];
}
