<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InscriptosFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nya' => 'required',
            'dni' => 'required|numeric',
            'ciudad' => 'required',
            'email' => 'required|email',
            'caracter' => 'required',
            'contacto' => 'required|numeric',
            'metodo_pago' => 'required'

        ];
    }

    public function messages()
    {
        return [
            'dni.required' => 'DNI obligatorio',
            'dni.unique' => 'El DNI ya se encuentra registrado',
            'contacto.numeric' => 'Solo se aceptan valores numericos',
            'contacto.required' =>'Contacto obligatorio',
            'nya.required' => 'Nombre y Apellido obligatorio',
            'ciudad.required' => 'Ciudad obligatorio',
            'email.required' => 'Email obligatorio',
            'email.email' => 'Debe ingresar un mail valido',
            'metodo_pago.required' => 'Debe seleccionar una forma de pago'


        ];
    }
}
