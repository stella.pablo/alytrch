<?php

namespace App\Http\Controllers;

use App\Curso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Image;

class CursosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $cursos = Curso::orderBy('created_at', 'DESC')->get();

        return view('cursos.index', compact('cursos'));
    }

    public function create()
    {

        return view('cursos.create');
    }

    public function store(Request $request)
    {

        $file = $this->saveFile($request);

        $doc = Curso::create([
            'titulo' => $request->titulo,
            'descripcion' => $request->descripcion,
            'fecha' => $request->fecha,
            'cupo' => $request->cupo,
            'imagen' => $file
        ]);

        return back();


    }

    public function edit(Curso $curso)
    {

        return view('cursos.edit', compact('curso'));
    }

    public function update(Request $request, $id)
    {

        $file = $this->saveImagen($request);

        $curso = Curso::where('id', '=', $id)->update([
            'titulo' => $request->titulo,
            'descripcion' => $request->descripcion,
            'fecha' => $request->fecha,
            'cupo' => $request->cupo,
            'imagen' => $file
        ]);

        return back();
    }


    public function saveFile(Request $request)
    {

        $file = $request->file('imagen');
        $ext = $file->guessClientExtension();


        $file->storeAs('cursos', "test.{$ext}", 'cursos');

        return "test.{$ext}";
    }


    public function saveImagen(Request $request)
    {

        if ($request->hasFile('imagen')) {
            $image = $request->file('imagen');
            $fileName = time() . '.' . $image->getClientOriginalExtension();

            $img = \Intervention\Image\Facades\Image::make($image->getRealPath());

            $img->resize(120, 120, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point

            //dd();
            Storage::disk('public')->put('images/1/smalls' . '/' . $fileName, $img, 'public');
        }
    }
}
