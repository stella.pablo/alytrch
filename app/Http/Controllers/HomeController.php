<?php

namespace App\Http\Controllers;

use App\Curso;
use App\Http\Requests\InscriptosFormRequest;
use App\Http\Requests\ComprobanteFormRequest;
use App\Inscripto;
use Carbon\Carbon;
use Illuminate\Http\Request;
use MercadoPago\MercadopagoSdkTest;
use SantiGraviano\LaravelMercadoPago\Facades\MP;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index(){

        $cursos = Curso::orderBy('created_at','DESC')->get();

        return view('home.cursos',compact('cursos'));
    }

    public function inscripcion(){

        $caracter = ['Profesional'=>'Profesional','Socios con deuda'=>'Socios con deuda','Socios al dia'=>'Socios al dia','Estudiante'=>'Estudiante'];

        $metodo_pago = [''=>'','1'=>'Pago en Asociacion','2'=>'Transferencia Bancaria','3'=>'Mercado Pago'];

        return view('home.inscripcion',compact('caracter','metodo_pago'));
    }

    public function store(InscriptosFormRequest $request){

        $fiesta = null;

        if($request->check){
           $fiesta = 500;
        }

        $inscripto = Inscripto::create([
            'nya' => $request->nya,
            'dni' => $request->dni,
            'ciudad' => $request->ciudad,
            'email' => $request->email,
            'caracter' => $request->caracter,
            'contacto' => $request->contacto,
            'titulo' => $request->titulo,
            'metodo_pago' => $request->metodo_pago,
            'pago' => $request->pago,
            'curso_id' => 7
        ]);


        if($request->caracter == 'Socios al dia'){
            $precio = 500;
        }else if($request->caracter == 'Estudiante'){
            $precio = 700;
        }else{
            $precio = 1000;
        }

        if($request->metodo_pago == 3){

            $preferenceData = [
                'items' => [
                    [
                        'external_reference' => $inscripto->id,
                        'category_id' => 'Congreso',
                        'title' => '36º Congreso Argentino de Licenciados y Técnicos Radiólogos',
                        'description' => '36º Congreso Argentino de Licenciados y Técnicos Radiólogos',
                        'id' => 1,
                        'picture_url' => 'http://alytrch.terobit.com/template/img/banner-ejemplo.png',
                        'quantity' => 1,
                        'currency_id' => 'ARS',
                        'unit_price' => $precio +$fiesta,
                    ]
                ],
                'back_urls' => [
                    'success'=>'http://alytrch.terobit.com',
                    'pending'=>'http://alytrch.terobit.com',
                    'failure'=>'http://alytrch.terobit.com',

                ],
            ];

            $preference = MP::create_preference($preferenceData);

            $inscripto->ml_id = $preference['response']['collector_id'];
            $inscripto->save();


            return redirect($preference['response']['init_point']);

        }

        return redirect('exito');
    }


    public function getPayments(){

        $mp = \MercadoPago\SDK::setAccessToken(" TEST-4571309705227362-072518-53f9e32ff836f01d0d0a63851d8d2ed3-333914545");

        $merchant_order = null;

        switch($_GET["topic"]) {
            case "payment":
                $payment = \MercadoPago\Payment::find_by_id($_GET["id"]);
                // Get the payment and the corresponding merchant_order reported by the IPN.
                $merchant_order = \MercadoPago\MerchantOrder::find_by_id($payment->order_id);
            case "merchant_order":
                $merchant_order = \MercadoPago\MerchantOrder::find_by_id($_GET["id"]);
        }

        foreach ($merchant_order->payments as $payment) {
            $paid_amount = 0;
            if ($payment['status'] == 'approved'){
                $paid_amount += $payment['transaction_amount'];
            }
        }

        // If the payment's transaction amount is equal (or bigger) than the merchant_order's amount you can release your items
        if($paid_amount >= $merchant_order->total_amount){
            if (count($merchant_order->shipments)>0) { // The merchant_order has shipments
                if($merchant_order->shipments[0]->status == "ready_to_ship") {
                    print_r("Totally paid. Print the label and release your item.");
                }
            } else { // The merchant_order don't has any shipments
                print_r("Totally paid. Release your item.");
            }
        } else {
            print_r("Not paid yet. Do not release your item.");
        }

        return response('','200');

    }

    public function exito(){
        return view('home.exito');
    }

    public function noregistrado(){
        return view('home.registrado');
    }

    public function getComprobante($id){

        $inscripto = Inscripto::find($id);
    
        $view = \View::make('pdf.comprobante',compact('inscripto'))->render();
        $pdf = \App::make('dompdf.wrapper');

        return $pdf->loadHTML($view)->setPaper('commercial #10 envelope', 'h')
            ->setWarnings(false)
            ->stream('invoice');

    }

    public function consultar(){

        return view('home.consultar');
    }

    public function certificado(){

        return view('home.certificado');
    }


    public function getComprobanteByForm(ComprobanteFormRequest $request){

        $inscripto = Inscripto::where('dni','=',$request->dni)->first();

        if($inscripto == null){
            return redirect('noregistrado');
        }

        $view = \View::make('pdf.comprobante',compact('inscripto'))->render();
        $pdf = \App::make('dompdf.wrapper');

        return $pdf->loadHTML($view)->setPaper('commercial #10 envelope', 'h')
            ->setWarnings(false)
            ->stream('invoice');

    }

    public function getInscriptos(){

        $inscriptos = Inscripto::orderBy('created_at','DESC')->get();


        $view = \View::make('pdf.comprobante',compact('inscripto'))->render();
        $pdf = \App::make('dompdf.wrapper');

        return $pdf->loadHTML($view)->setPaper('commercial #10 envelope', 'h')
            ->setWarnings(false)
            ->stream('invoice');


    }

    public function getCertificado(ComprobanteFormRequest $request){


        $inscripto = Inscripto::where('dni',$request->dni)->where('pago',1)->first();


        if($inscripto == null){
            return redirect()->back()->with('success', 'No se encuentra habilitado para emitir el certificado');

        }


        $date = Carbon::now();

        $view = \View::make('pdf.certificado',compact('inscripto','date'))->render();
        $pdf = \App::make('dompdf.wrapper');

        return $pdf->loadHTML($view)->setPaper('a4', 'landscape')
            ->setWarnings(false)
            ->stream('invoice');


    }
}
