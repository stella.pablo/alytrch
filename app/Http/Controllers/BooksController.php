<?php

namespace App\Http\Controllers;

use App\Book;
use App\Http\Requests\CreateFormBookRequest;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    public function index(){

        $books = Book::all();

        return compact('books.index',compact('books'));

    }

    public function create(){

        return view('books.create');

    }

    public function store(CreateFormBookRequest $request){

        $book = Book::create($request->all());

    }

    public function edit($id){

        $book = Book::find($id)->get();

        dd($book);


    }

}
