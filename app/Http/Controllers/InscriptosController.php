<?php

namespace App\Http\Controllers;

use App\Inscripto;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Psr\Log\NullLogger;

class InscriptosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $inscriptos = Inscripto::where('curso_id','=','7')->orderBy('created_at','DESC')->get();


        return view('inscriptos.index',compact('inscriptos'));
    }

    public function edit(Inscripto $inscripto)
    {

        $caracter = ['Profesional'=>'Profesional','Socios con deuda'=>'Socios con deuda','Socios al dia'=>'Socios al dia',
        'Estudiante'=>'Estudiante','Disertante'=>'Disertante','Organizador'=>'Organizador',];

        $metodo = ['Efectivo'=>        'Efectivo','Transferencia'=>'Transferencia','Tarjeta'=>'Tarjeta'];

        return view('inscriptos.edit',compact('inscripto','caracter','metodo'));
    }

    public function update(Request $request, $id){

        //$inscripto = Inscripto::where('id',$id)->first();

        if(isset($request->pago)){

            $request->pago = 1;

        }else{

            $request->pago = 0;
        }


        $doc = Inscripto::where('id',$id)->update([
            'nya' => $request->nya,
            'dni' => $request->dni,
            'ciudad' => $request->ciudad,
            'email' => $request->email,
            'caracter' => $request->caracter,
            'contacto' => $request->contacto,
            'titulo' => $request->titulo,
            'metodo_pago' => $request->metodo_pago,
            'pago' => $request->pago
        ]);


        //$inscripto->fill($request->input())->save();

        return redirect()->route('inscriptos.index');
    }

    public function destroy($id){

        $doc = Inscripto::find($id)->delete();


        return back();

    }

    public function getInscriptos(){

        $inscriptos = Inscripto::orderBy('created_at','DESC')->get();


        $view = \View::make('pdf.listado',compact('inscriptos'))->render();
        $pdf = \App::make('dompdf.wrapper');

        return $pdf->loadHTML($view)->setPaper('legal', 'h')
            ->setWarnings(false)
            ->stream('invoice');


    }

    public function getCertificado($dni){

        $inscripto = Inscripto::where('dni',$dni)->first();


        $date = Carbon::now();


        $view = \View::make('pdf.certificado',compact('inscripto','date'))->render();
        $pdf = \App::make('dompdf.wrapper');

        return $pdf->loadHTML($view)->setPaper('a4', 'landscape')
            ->setWarnings(false)
            ->stream('invoice');


    }
}
