<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inscripto extends Model
{

    protected $table ='inscriptos';

    protected  $fillable = ['nya','apellido','dni','ciudad','email','caracter','contacto','titulo','metodo_pago','pago','ml_id','curso_id'];
}
