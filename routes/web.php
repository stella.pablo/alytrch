<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('cursos-disponibles','CursosController@index');

Route::post('cursos/store',['as'=>'cursos.almacenar','uses'=>'CursosController@store']);

Route::get('notificaciones', 'HomeController@getPayments');

//emitir comprobante
Route::get('consultar-inscripcion', 'HomeController@consultar')->name('consultar');
Route::post('consultar-inscripcion',['as'=>'getcomprobante','uses'=>'HomeController@getComprobanteByForm']);

//listado de inscriptos
Route::get('listado',['as'=>'getinscriptos','uses'=>'InscriptosController@getInscriptos']);

//emitir certificado
Route::get('emitir-certificado', 'HomeController@certificado')->name('certificado');
Route::post('certificado',['as'=>'getcertificado','uses'=>'HomeController@getCertificado']);


Route::get('comprobante/{id}', 'HomeController@getComprobante')->name('comprobante');

Route::resource('cursos', 'CursosController');


Route::get('/', 'HomeController@index')->name('home');

Route::get('inscripcion', 'HomeController@inscripcion')->name('inscripcion');



Route::post('inscripcion/store',['as'=>'inscripcion.store','uses'=>'HomeController@store']);

Route::get('inscriptos/{id}/delete',['as'=>'inscriptos.delete','uses'=>'InscriptosController@destroy']);



Route::resource('inscriptos', 'InscriptosController');

Auth::routes();

Route::get('home', 'HomeController@index')->name('home');
Route::get('exito', 'HomeController@exito')->name('exito');
Route::get('noregistrado', 'HomeController@noregistrado')->name('noregistrado');

//books
Route::get('books','BooksController@index');
Route::get('books/create','BooksController@create');
Route::post('books',['as'=>'books.store','uses'=>'BooksController@store']);
Route::get('books/{book}/edit','BooksController@edit');
Route::put('books/{book}','BooksController@update');























