-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 03-10-2019 a las 23:45:55
-- Versión del servidor: 5.7.27-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `radiologos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cupo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `finalizo` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id`, `titulo`, `descripcion`, `fecha`, `imagen`, `cupo`, `finalizo`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, '36º Congreso Argentino de Licenciados y Técnicos Radiólogos', 'Una nueva edición de este evento de actualización tan importante para el sector radiológico se llevará a cabo en la Provincia del Chaco del 8 al 10 de Noviembre de 2018. La sede será el Domo del Centenario de la Ciudad de Resistencia. Como siempre, habrá interesantes talleres con una diversidad de temas.', '2018-07-10', NULL, '100', 1, '2018-07-18 22:41:24', '2018-12-04 20:59:24', NULL),
(6, 'III Capacitacion Radiologica ', 'III Capacitacion Radiologica en el interior del Chaco.\r\nEntrada, alimento no perecedero', '2018-12-15', 'castelli.jpg', '100', NULL, '2018-12-04 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscriptos`
--

CREATE TABLE `inscriptos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nya` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dni` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ciudad` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caracter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contacto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ml_id` int(11) DEFAULT NULL,
  `curso_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pago` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metodo_pago` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `inscriptos`
--

INSERT INTO `inscriptos` (`id`, `nya`, `dni`, `ciudad`, `email`, `caracter`, `titulo`, `contacto`, `ml_id`, `curso_id`, `created_at`, `updated_at`, `pago`, `metodo_pago`) VALUES
(11, 'Sena Loana Marina', '39861589', 'Corrientes', 'loanamarinasena@hotmail.com', 'Estudiante', 'Técnico superior en Diagnóstico por imagenes', '3781490958', NULL, NULL, '2018-07-25 15:24:46', '2018-07-25 15:24:46', NULL, NULL),
(12, 'Nunez Jorge Ezequiel', '40048686', 'Corrientes', 'jorgitoeze@hotmail.com', 'Profesional', 'Técnico Superior en Diagnóstico por Imagenes', '3773409250', NULL, NULL, '2018-07-25 15:27:05', '2018-07-25 15:27:05', NULL, NULL),
(13, 'Abalos nidia', '24420336', 'Santa lucia corrientes', 'Libe1952@gmail.com', 'Profesional', 'Tecnica radiologa', '3777595734', NULL, NULL, '2018-07-30 16:18:21', '2018-07-30 16:18:21', NULL, NULL),
(14, 'Rios Micaela Belen', '37862683', 'Bahía Blanca', 'Micaela.rx@gmail.com', 'Profesional', 'Técnico Superior en Salud con Especialización en Radiología', '0291155321002', NULL, NULL, '2018-08-04 13:38:50', '2018-08-04 13:38:50', NULL, NULL),
(18, 'Cordoba Daiana Noemi', '32048484', 'Formosa, Capital', 'dcordoba@osjera.com.ar', 'Estudiante', 'Estudiante de Tecnico en Radiologia', '3704822793', 333914545, NULL, '2018-08-13 15:44:10', '2018-08-13 15:44:11', NULL, '2'),
(20, 'Ocampo diego', '34942917', 'Obera misiones', 'ochieggo@gmail.com', 'Estudiante', 'Tec. Radiologo', '03755504862', NULL, NULL, '2018-08-14 02:19:48', '2018-08-14 02:19:48', NULL, '1'),
(21, 'Barreto sergio david', '24234610', 'Punta Alta', 'ser_rx@yahoo.com.ar', 'Organizador', 'lic. en produccion de bioimagenes', '0293215497205', NULL, NULL, '2018-08-14 14:40:41', '2018-11-19 12:41:28', '1', 'Efectivo'),
(24, 'Biscay Bernardo', '29660476', 'Colonias Unidas', 'bernarcarp_22@hotmail.com', 'Profesional', 'Técnico Radiólogo', '3725608196', NULL, NULL, '2018-08-17 04:59:22', '2018-11-14 11:47:41', '1', 'Efectivo'),
(25, 'Goitia sandra beatriz', '24297003', 'Resistencia chaco', 'Sandry_goitia@hotmail.com', 'Socios al dia', 'Tecnica radiologa', '3624813924', NULL, NULL, '2018-08-19 00:19:15', '2018-11-16 13:31:51', '1', 'Efectivo'),
(26, 'Acevedo Fernando', '23382661', 'Resistencia', 'Fertorques@gmail.com', 'Socios al dia', 'Ninguno', '3624682637', 333914545, NULL, '2018-08-19 02:11:18', '2018-11-16 19:04:07', '0', 'Efectivo'),
(27, 'Sandoval Roxana Beatriz', '32854607', 'Resistencia', 'rox29sandoval@hotmail.com', 'Profesional', 'Tecnico En Radiologia', '3624593505', NULL, NULL, '2018-08-22 01:58:36', '2018-11-12 14:28:35', '1', 'Efectivo'),
(32, 'Sanchez Dania Yamila', '36270671', 'Resistencia', 'daniamartinlaza@gmail.com', 'Profesional', 'Tecnico superior en radiologia', '3624028266', NULL, NULL, '2018-08-22 23:40:24', '2018-11-16 14:44:38', '1', 'Efectivo'),
(33, 'Mendez ELizabeth Viviana', '28147242', 'Corrientes', 'elimendez1980@gmail.com', 'Estudiante', 'diagnostico por imagen', '1166620762', 333914545, NULL, '2018-08-22 23:49:06', '2018-08-22 23:49:07', NULL, '2'),
(34, 'Zarinsky gladis lilian', '25276147', 'Resistencia chaco', 'Sandry_goitia@hotmail.com', 'Socios al dia', 'Tecnica radiologa', '3624540012', NULL, NULL, '2018-08-23 00:10:51', '2018-11-16 13:30:59', '1', 'Efectivo'),
(35, 'Alarcon ivana', '32486757', 'Resistencia', 'ivanamarisolalarcon29@gmail.com', 'Profesional', 'Técnica radiologia', '3721410715', 333914545, NULL, '2018-08-23 00:46:35', '2018-11-20 20:26:08', '1', 'Efectivo'),
(40, 'Rojas Noelia Gisel', '35450585', 'Corrientes', 'Shisyrojas@gmail.com', 'Estudiante', 'Técnico superior en Diagnóstico por imagenes', '3794083444', NULL, NULL, '2018-08-23 11:01:15', '2018-08-23 11:01:15', NULL, '1'),
(41, 'Wasser Hugo Hernan', '38234457', 'Corrientes', 'Wasserhernan3@gmail.com', 'Estudiante', 'Técnico superior en Diagnóstico por imagenes', '3794331519', NULL, NULL, '2018-08-23 11:03:57', '2018-08-23 11:03:57', NULL, '1'),
(42, 'Arias estefania', '38342796', 'Salta', 'ariasestefania796@gmail.com', 'Estudiante', 'Tecnico superior en Radiologia', '3875908227', NULL, NULL, '2018-08-23 18:30:35', '2018-08-23 18:30:35', NULL, '1'),
(43, 'Lopez Lucila', '39799398', 'San Carlos - Mendoza', 'lucilalopez_15@live.com.ar', 'Profesional', 'Técnico superior en radiología y diagnóstico por imágenes', '2622615179', 333914545, NULL, '2018-08-23 20:37:16', '2018-11-19 12:18:50', '1', 'Efectivo'),
(44, 'Almua cristina elisabet', '27377754', 'Saenz peña chaco', 'Cristinaalmua@635gmail.com', 'Estudiante', 'Estudiante', '3644592883', NULL, NULL, '2018-08-24 01:10:15', '2018-11-23 01:15:32', '1', 'Efectivo'),
(45, 'Campos Tatiana Belen', '42191498', 'Presidencia Roque Sáenz Peña', 'Tati54461@gmail.com', 'Estudiante', 'Estudiante', '3644706359', NULL, NULL, '2018-08-24 01:29:58', '2018-11-23 01:16:30', '1', 'Efectivo'),
(46, 'Balenzuela Adriana', '30410922', 'Presidencia Roque Sáenz Peña', 'graceadriana@hotmail.com.ar', 'Socios', 'Estudiante', '01166581375', NULL, NULL, '2018-08-24 01:32:33', '2018-08-24 01:32:33', NULL, '1'),
(47, 'Retamozo Rita', '31395949', 'Presidencia Roque Sáenz Peña', 'graceadriana@hotmail.com.ar', 'Estudiante', 'Estudiante', '3644114779', NULL, NULL, '2018-08-24 01:33:22', '2018-11-23 01:18:19', '1', 'Efectivo'),
(48, 'Coronel Susana', '34042020', 'Presidencia Roque Sáenz Peña', 'graceadriana@hotmail.com.ar', 'Estudiante', 'Estudiante', '3644443139', NULL, NULL, '2018-08-24 01:35:27', '2018-11-23 01:17:11', '1', 'Efectivo'),
(49, 'Cano Silvia', '31952070', 'Presidencia Roque Sáenz Peña', 'graceadriana@hotmail.com.ar', 'Organizador', 'Estudiante', '3644151073', NULL, NULL, '2018-08-24 01:36:32', '2018-11-28 17:00:03', '1', 'Efectivo'),
(50, 'Diaz Sergio Alonso', '28546920', 'Avia Terai', 'sergiodiaz_28@hotmail.com', 'Estudiante', 'Estudiante', '3644619985', NULL, NULL, '2018-08-24 01:54:36', '2018-11-23 01:14:51', '1', 'Efectivo'),
(51, 'Toloza Rosana Isabel', '23321687', 'Pcia. Roque Sáenz peña chaco', 'rosanaisabeltoloza@gmail.com', 'Organizador', 'estudiante', '3644741959', NULL, NULL, '2018-08-24 02:17:00', '2018-11-23 01:05:15', '1', 'Efectivo'),
(52, 'Frias Mabel Beatriz', '17465820', 'PCIA.R.SAENZ PEÑA', 'mabelfrias707@gmail.com', 'Estudiante', 'Estudiante', '3644343723', NULL, NULL, '2018-08-24 03:37:40', '2018-11-23 01:16:00', '1', 'Efectivo'),
(53, 'CUELLAR YANINA CLAUDIA', '31325997', 'FORMOSA', 'peritogna@hotmail.com', 'Estudiante', 'TÉCNICO SUPERIOR EN RADIOLOGÍA', '3704061150', NULL, NULL, '2018-08-24 13:57:49', '2018-08-24 13:57:49', NULL, '1'),
(54, 'Sanchez Clara irupe', '40281246', 'Corrientes', 'Clara.irupe.sanchez@gmail.com', 'Estudiante', 'Técnico superior en diagnóstico por imágenes', '3794650097', NULL, NULL, '2018-08-25 18:19:27', '2018-11-12 14:44:50', '1', 'Efectivo'),
(55, 'ramirez carla Vanesa', '30523357', 'resistencia', 'carlavanesaramirez820@gmail.com', 'Estudiante', 'secundario', '3624802433', NULL, NULL, '2018-08-25 18:30:36', '2018-08-25 18:30:36', NULL, '1'),
(56, 'Batalla Horacio Javier', '31109929', 'GENERAL JOSE DE SAN MARTIN', 'javiermichigan2009@gmail.com', 'Profesional', 'Tecnico Superior en Radiologia', '3624681672', 333914545, NULL, '2018-08-25 19:06:55', '2018-08-25 19:06:56', NULL, '2'),
(57, 'Martinez clara', '34294217', 'Formosa', 'martinezclara954@gmail.com', 'Profesional', 'Tecnica Radiologa', '3704650714', 333914545, NULL, '2018-08-25 19:12:59', '2018-11-16 13:26:46', '1', 'Efectivo'),
(58, 'acevedo monica elisabeth', '30214625', 'fontana', 'monicachaco44@gmail.com', 'Socios al dia', 'técnica radiologa', '3624223750', NULL, NULL, '2018-08-25 19:19:18', '2018-11-16 14:36:17', '1', 'Efectivo'),
(59, 'Cardozo Cinthia Vanesa', '30571858', 'Fontana Chaco', 'cinthia6715@gmail.com', 'Socios', 'Técnica radióloga', '3624622988', NULL, NULL, '2018-08-25 19:21:50', '2018-08-25 19:21:50', NULL, '1'),
(60, 'Gonzalez Erica', '40420584', 'Corrientes', 'ericap.gonzalez26@gmail.com', 'Estudiante', 'Tec superior en diagnóstico por imagenes', '3794759787', NULL, NULL, '2018-08-25 23:06:49', '2018-11-12 14:44:13', '1', 'Efectivo'),
(61, 'Sanchez Adriana Lorena', '36487218', 'Resistencia', 'adrianasaanchez@hotmail.com', 'Estudiante', 'Técnico Radiólogo', '03624150877', 333914545, NULL, '2018-08-25 23:26:35', '2018-08-25 23:26:36', NULL, '2'),
(62, 'Molina Alejandra Adelina', '36808761', 'Resistencia', 'Adeemolina5@gmail.com', 'Estudiante', 'Tecnico en radiología', '2966527616', NULL, NULL, '2018-08-26 00:14:24', '2018-08-26 00:14:24', NULL, '1'),
(63, 'Forte', '38195132', 'Pcia Roque Sáenz Peña', 'deborayakeline1422@gmail.com', 'Estudiante', 'Estudiante', '3644720798', NULL, NULL, '2018-08-26 12:14:12', '2018-08-26 12:14:12', NULL, '1'),
(64, 'Zuazquita Miliana yanina', '26136719', 'Prcia Roque Sáenz Peña', 'milizuazquita64@gmail.com', 'Socios', 'Lic en Radiología', '3644628557', NULL, NULL, '2018-08-27 00:36:12', '2018-08-27 12:33:02', '0', 'Efectivo'),
(65, 'Cesar mancuello', '33147955', 'Presidencia roque Sáenz peña', 'cesarmancuello2014@gmail.com', 'Estudiante', 'Estudiante', '3644363089', NULL, NULL, '2018-08-27 18:22:37', '2018-11-23 01:14:04', '1', 'Efectivo'),
(66, 'Gabriela Mariana sarmiento', '31503188', 'Presidencia roque Sáenz peña', 'cesarmancuello2014@gmail.com', 'Socios', 'Estudiante', '3644364527', NULL, NULL, '2018-08-27 18:24:07', '2018-08-27 18:24:07', NULL, '1'),
(67, 'Luis andres encina', '24931703', 'Presidencia roque Sáenz peña', 'cesarmancuello2014@gmail.com', 'Estudiante', 'Estudiante', '3644314022', NULL, NULL, '2018-08-27 18:25:52', '2018-11-16 13:58:19', '1', 'Efectivo'),
(68, 'Alejandra debora fernandez', '25278301', 'Presidencia roque Sáenz peña', 'cesarmancuello2014@gmail.com', 'Estudiante', 'Estudiante', '3644220527', NULL, NULL, '2018-08-27 18:28:23', '2018-11-23 01:11:35', '1', 'Efectivo'),
(69, 'Gomez Jonathan', '39314610', 'Barranqueras', 'rolandogomez2015@outlook.com', 'Estudiante', 'Técnico superior en Radiología', '3794601153', NULL, NULL, '2018-08-28 00:01:11', '2018-08-28 00:01:11', NULL, '1'),
(70, 'Ponce Brenda Diamela', '40974562', 'Resistencia', 'brenda.ponce.chaco@gmail.com', 'Estudiante', 'Técnico superior en Radiología', '3624557983', NULL, NULL, '2018-08-28 00:02:26', '2018-08-28 00:02:26', NULL, '1'),
(71, 'SAID MARIA DEL CARMEN', '4909138', 'Luque -Paraguay', 'marichu_said@hotmail.com', 'Profesional', 'Licenciada', '+595961906291', 333914545, NULL, '2018-08-28 16:11:00', '2018-08-28 16:11:01', NULL, '2'),
(72, 'Yasnikouski Tamara Ayelen', '41294634', 'Concepción de la Siarra', 'aye.yasnikouski@gmail.com', 'Estudiante', 'TECNICATURA DE DIAGNOSTICO PÓR IMÁGENES', '3758485332', 333914545, NULL, '2018-08-28 18:07:18', '2018-11-23 21:36:56', '1', 'Efectivo'),
(73, 'Suarez Gisela Estefania', '37394163', 'Corrientes', 'giselavalen11@gmail.com', 'Estudiante', 'Técnico superior en diagnóstico por imagenes', '3795346341', NULL, NULL, '2018-08-29 13:47:26', '2018-11-14 12:21:34', '1', 'Efectivo'),
(74, 'lopez veronica', '34655009', 'corrientes', 'viruja_ml@hotmail.com', 'Estudiante', 'técnico en diagnostico por imagen', '3794343371', NULL, NULL, '2018-08-29 14:15:19', '2018-08-29 14:15:19', NULL, '1'),
(75, 'Linares Zulma Raquel', '27572255', 'Resistencia', 'zunam295@gmail.com', 'Estudiante', 'Técnico Radiologo', '3624382155', NULL, NULL, '2018-08-29 14:31:28', '2018-11-17 10:31:09', '1', 'Efectivo'),
(76, 'Carlos waldemar gaston monzon', '29020381', 'Presidencia roque Sáenz peña', 'cesarmancuello2014@gmail.com', 'Estudiante', 'Estudiante', '3644273699', NULL, NULL, '2018-08-29 16:07:45', '2018-11-23 01:18:51', '1', 'Efectivo'),
(77, 'Cenoz Maria Paz', '38315566', 'Corrientes', 'pachucenoz@hotmail.com', 'Estudiante', 'Tecnico radiologo', '+543794815137', NULL, NULL, '2018-08-30 11:55:16', '2018-11-12 14:43:30', '1', 'Efectivo'),
(78, 'Aquino Carola Valentina', '40701582', 'Corrientes', 'valen.c.aquino@gmail.com', 'Estudiante', 'Técnico radiólogo', '3773418843', NULL, NULL, '2018-08-30 11:55:17', '2018-11-12 14:40:58', '1', 'Efectivo'),
(79, 'Velazquez Florencia Ines', '40507456', 'Corrientes', 'Florenvelazq@gmail.com', 'Estudiante', 'Tecnico radiologo', '3794845019', NULL, NULL, '2018-08-30 11:56:58', '2018-11-12 14:41:45', '1', 'Efectivo'),
(80, 'Otto Santiago Javier', '37718944', 'Corrientes', 'santiagootto44@gmail.com', 'Estudiante', 'Técnico radiólogo', '3755607853', NULL, NULL, '2018-08-30 13:09:55', '2018-11-12 14:27:03', '1', 'Efectivo'),
(81, 'Lopez Maria Vanesa', '30295433', 'Resistencia', 'vanesamlopez@hotmail.com.ar', 'Profesional', 'Tec. Radiologa', '3624145774', NULL, NULL, '2018-08-30 17:35:56', '2018-11-12 15:40:27', '1', 'Efectivo'),
(82, 'Bojorque Romina', '41924319', 'Corrientes', 'bojorqueromina@gmail.com', 'Estudiante', 'Diagnóstico por imágenes', '3794594626', NULL, NULL, '2018-08-30 18:00:30', '2018-11-16 13:49:41', '1', 'Efectivo'),
(83, 'Sandoval Mercedes Ramona', '36388315', 'San luis del palmar', 'Mercedesandoval1991@gmail.com', 'Estudiante', 'Diagnóstico por imagen', '3794212886', NULL, NULL, '2018-08-31 00:38:46', '2018-11-16 13:48:55', '1', 'Efectivo'),
(84, 'cecotto carolina elisa', '28781650', 'corrientes', 'salmos23karpiel@gmail.com', 'Profesional', 'licenciado en radiología e imágenes', '3794680155', 333914545, NULL, '2018-08-31 11:55:31', '2018-11-12 11:56:54', '1', 'Efectivo'),
(86, 'ALFONZO ROSA DEL CARMEN', '29672989', 'CORRIENTES', 'rocaalf82@gmail.com', 'Profesional', 'TECNICA EN DIAGNOSTICO POR IMAGEN', '3794922204', 333914545, NULL, '2018-08-31 12:08:37', '2018-11-14 12:21:59', '1', 'Efectivo'),
(87, 'Chapero Sofia', '38816115', 'Santa Fe', 'schapero95@hotmail.com', 'Estudiante', 'Tecnico en Diagnostico por imagenes', '3482588653', NULL, NULL, '2018-09-04 02:16:17', '2018-11-17 10:35:26', '1', 'Efectivo'),
(88, 'Artin Rodrigo', '38310442', 'Corrientes Capital', 'artinrodrigo@gmail.com', 'Profesional', 'Técnico en Diagnóstico por Imágenes', '3794342575', 333914545, NULL, '2018-09-04 19:33:08', '2018-09-11 02:21:02', NULL, '2'),
(89, 'Diaz Lourdes Elizabeth', '39134972', 'Formosa', 'loudiaz18@gmail.com', 'Profesional', 'Técnico Superior en Radiología', '3704021303', 333914545, NULL, '2018-09-05 16:44:44', '2018-11-17 10:27:34', '1', 'Efectivo'),
(90, 'Galvan Julio', '23740414', 'caleta olivia', 'tacjulio@hotmail.com', 'Profesional', 'Tecnico Radiologo', '2974046485', NULL, NULL, '2018-09-07 13:43:36', '2018-09-07 13:43:36', NULL, '1'),
(91, 'Morales Milena', '41807756', 'Corrientes', 'mile1799@outlook.es', 'Estudiante', 'Técnico Superior en Diagnóstico por Imágenes', '3794275652', NULL, NULL, '2018-09-08 15:37:28', '2018-09-08 15:37:28', NULL, '1'),
(92, 'Silvia laura morla', '29149750', 'Pirane', 'Silviafran2016@gmail.com', 'Estudiante', 'Tecnica radiologa', '3704269113', NULL, NULL, '2018-09-09 16:23:56', '2018-09-09 16:23:56', NULL, '1'),
(93, 'Morales Rocio Maive', '37479450', 'Puerto Vilelas, Chaco', 'roci016@hotmail.com', 'Profesional', 'Técnico Superior en Radiologia', '3624733536', NULL, NULL, '2018-09-10 14:30:50', '2018-11-16 14:37:00', '1', 'Efectivo'),
(94, 'Dilchoff Iara Jackeline', '40702998', 'Resistencia', 'jdilchoff@gmail.com', 'Estudiante', 'Tecnico Radiologo', '3482616251', 333914545, NULL, '2018-09-11 17:46:37', '2018-11-16 13:19:35', '1', 'Efectivo'),
(95, 'Ojeda Bettiana Giselle', '37593272', 'Resistencia', 'bgso22@yahoo.com.ar', 'Profesional', 'Tec. Superior en Radiología', '3624687637', NULL, NULL, '2018-09-12 11:13:29', '2018-09-12 11:13:29', NULL, '1'),
(96, 'Nancy Ruiz diaz', '28943967', 'Makalle', 'ruizdiaznancy88@gmail.com', 'Socios al dia', 'Técnico radiologo', '3624788822', NULL, NULL, '2018-09-12 16:54:21', '2018-11-12 14:33:07', '1', 'Efectivo'),
(97, 'ZALAZAR VERONICA NATALIA', '36514950', 'Corrientes', 'Veronica.n.zalazar@hotmail.com', 'Profesional', 'Tec. En diagnostico por imagen', '3794547046', NULL, NULL, '2018-09-12 21:20:44', '2018-09-12 21:20:44', NULL, '1'),
(98, 'Quevedo Jorge daniel', '26378156', 'Villa Ocampo provincia santa fe', 'danielquevedo110478@hotmail.com', 'Profesional', 'Técnico superior en radiología', '0348215649777', NULL, NULL, '2018-09-14 14:31:45', '2018-11-23 21:20:36', '1', 'Efectivo'),
(99, 'Lucero  carla', '24447840', 'Mendoza', 'aixacarla@gmail.com', 'Profesional', 'Lic. En producción de bio imágenes', '2616723080', NULL, NULL, '2018-09-14 20:25:06', '2018-09-14 20:25:06', NULL, '1'),
(100, 'Farias Eliana', '37262584', 'Resistencia', 'ayelen_piccoli@hotmail.com', 'Estudiante', 'Radiología', '3625238787', NULL, NULL, '2018-09-14 20:55:57', '2018-09-14 20:55:57', NULL, '1'),
(101, 'Garcia villan horacio damin', '36548109', 'Corrientes', 'horacio211291@gmail.com', 'Estudiante', 'Tecnico superior en diagnostico por imagen', '3794343786', NULL, NULL, '2018-09-15 06:12:38', '2018-11-16 13:46:43', '1', 'Efectivo'),
(102, 'cabrera rocio antonia', '41249250', 'caa cati-corrientes', 'cabrerarocio1709@gmail.com', 'Estudiante', 'Tecnico superior en diagnostico por imagen', '3781404919', NULL, NULL, '2018-09-15 18:18:01', '2018-09-15 18:18:01', NULL, '1'),
(103, 'Marquez Diana Belen', '39778984', 'Corrientes Capital', 'dianiita_beel@hotmail.com', 'Estudiante', '-', '3794543986', NULL, NULL, '2018-09-18 00:26:06', '2018-11-16 13:42:02', '1', 'Efectivo'),
(104, 'Difilipo Tamara', '34043399', 'Resistencia', 'tamaraelizabethdifilipo@outlook.com', 'Estudiante', 'Técnico radiologo', '3624784788', NULL, NULL, '2018-09-18 15:14:47', '2018-09-18 15:14:47', NULL, '1'),
(105, 'Lugo valeria Andrea', '30196826', 'Villa Angela', 'Valeriaalugoo@hotmail.com', 'Profesional', 'Técnico en diagnóstico superior por imagen', '3735452388', NULL, NULL, '2018-09-19 02:03:02', '2018-11-12 15:45:23', '1', 'Efectivo'),
(106, 'Lugo silvia viviana', '27584811', 'Villa Angela chaco', 'Silviavivianalugo@gmail.com', 'Profesional', 'Técnica radiologa', '3735405663', NULL, NULL, '2018-09-19 02:06:26', '2018-11-12 15:44:52', '1', 'Efectivo'),
(107, 'Meza Aldana Daniela', '40806028', 'Corrientes', 'aldana-meza@hotmail.com', 'Estudiante', 'Técnico en diagnóstico por imágenes', '3455087063', NULL, NULL, '2018-09-19 13:00:30', '2018-11-12 14:26:24', '1', 'Efectivo'),
(108, 'baez rosario trinidad', '41159372', 'itati-corrientes', 'trinyrosario2309@gmail.com', 'Organizador', 'Tecnico superior en diagnostico por imagen', '3794680909', NULL, NULL, '2018-09-19 16:23:29', '2018-11-12 15:48:01', '1', 'Efectivo'),
(109, 'Viñuela vanesa', '27990375', 'Resistencia', 'vanevinuela76@gmail.com', 'Profesional', 'Licenciado en radiología', '3624385836', NULL, NULL, '2018-09-19 19:44:58', '2018-11-16 13:13:32', '1', 'Efectivo'),
(110, 'Ceballo luciano benjamin', '36194197', 'Corrientes capital', 'Lbc912@hotmail.com', 'Estudiante', 'Tecnico superior en diagnostico por imagenes', '3794582530', NULL, NULL, '2018-09-19 20:14:04', '2018-11-16 13:42:34', '1', 'Efectivo'),
(111, 'Rolon Carlos', '30166948', 'Resistencia', 'cjr_07@hotmail.com', 'Profesional', 'Tec Radiologo', '3624628261', NULL, NULL, '2018-09-20 12:15:33', '2018-11-16 13:10:27', '1', 'Efectivo'),
(112, 'ROSATTO ELAINE SOLEDAD', '34890410', 'Monte caseros corriente', 'Hugo1550@hotmail.com', 'Profesional', 'Radiologa', '3794543660', NULL, NULL, '2018-09-21 12:01:48', '2018-11-14 12:32:34', '1', 'Efectivo'),
(113, 'Diana Ferreira Bareiro', '4429419', 'Asunción _ Paraguay', 'dfbareiro@gmail.com', 'Profesional', 'Licenciada en Radiología', '+595972410783', 333914545, NULL, '2018-09-22 02:32:12', '2018-11-19 12:34:40', '1', 'Efectivo'),
(114, 'Britez Carla Malena', '42717407', 'Corrientes-Corrientes', 'malenabritez00@gmail.com', 'Estudiante', 'Técnico Superior en Diagnóstico por Imágenes', '3755687948', NULL, NULL, '2018-09-22 20:39:41', '2018-11-16 13:45:41', '1', 'Efectivo'),
(115, 'Alarcon Silvana Vanesa', '26432842', 'Corrientes', 'silvana210378@hotmail.com.ar', 'Profesional', 'Técnica Radióloga', '3794607868', 333914545, NULL, '2018-09-22 23:44:55', '2018-11-20 18:39:54', '1', 'Tarjeta'),
(116, 'Santiago Vega Caballero', '4002042', 'Asunción _ Paraguay', 'dfbareiro@gmail.com', 'Profesional', 'Licenciado en Radiología', '+595972410783', 333914545, NULL, '2018-09-23 00:35:26', '2018-11-17 10:18:33', '1', 'Efectivo'),
(117, 'Calvano Noelia', '34298575', 'Corrientes', 'noelia123calvano@gmail.com', 'Estudiante', 'Diag por imagen', '03794779966', 333914545, NULL, '2018-09-23 20:38:42', '2018-09-23 20:38:43', NULL, '2'),
(118, 'Zarza Leandro Gaston', '38141795', 'Formosa', 'gastn@live.com.ar', 'Profesional', 'Tec. Superior en Radiología', '3704095406', NULL, NULL, '2018-09-25 08:40:54', '2018-11-14 12:23:20', '1', 'Efectivo'),
(119, 'Gassman Micaela', '34617354', 'Juan Jose Castelli- Departamento Gemeral Gûemes', 'micaelarx24@hotmail.com', 'Profesional', 'técnica superior en Diagnostico por imágenes', '0364154341444', 333914545, NULL, '2018-09-25 15:11:23', '2018-11-16 13:14:31', '1', 'Efectivo'),
(120, 'Juncos Lidia Gloria', '20997187', 'Cordoba capital', 'graco2008@live.com.ar', 'Profesional', 'Lic. en Produccion de bio imagenes', '3513410005', NULL, NULL, '2018-09-25 17:58:55', '2018-11-16 13:25:06', '1', 'Efectivo'),
(121, 'Collante  Graciela', '17530493', 'Cordoba capital', 'graco2008@live.com.ar', 'Profesional', 'Lic. en Produccion de bio imagenes', '3512625970', 333914545, NULL, '2018-09-25 17:59:53', '2018-11-16 13:24:34', '1', 'Efectivo'),
(122, 'Caceres Maria Cecilia', '36548190', 'Corrientes', 'ceciimca@gmail.com', 'Estudiante', 'Técnico Superior en Diagnostico por Imágenes', '03794529054', 333914545, NULL, '2018-09-26 16:30:18', '2018-11-16 13:13:53', '1', 'Efectivo'),
(123, 'Andrea C Villanueva', '35466382', 'Resistencia', 'carolina.villanueva@live.com', 'Socios al dia', 'Tec. Radiologo', '3624070999', NULL, NULL, '2018-09-26 16:44:55', '2018-11-16 14:37:57', '1', 'Efectivo'),
(124, 'Moralez mauro daniel', '31112373', 'Resistencia chaco', 'Plu-k_22@hotmail.com', 'Profesional', 'radiologia', '3624389940', NULL, NULL, '2018-09-26 18:43:02', '2018-11-16 15:43:38', '1', 'Efectivo'),
(125, 'JUAREZ FADEL ILDA', '11282820', 'SALTA', 'ilda_37@hotmail.com', 'Socios al dia', 'LIC.PRODUCC.BIOIMAGENED', '3876057577', NULL, NULL, '2018-09-26 19:32:45', '2018-11-17 10:15:34', '1', 'Efectivo'),
(126, 'Aguirre Natalia Ayelen', '38135094', 'Villa Ocampo- Santa Fe', 'Ayelenaguirre11@gmail.com', 'Estudiante', 'Secundario', '3482621188', NULL, NULL, '2018-09-26 20:55:34', '2018-11-12 15:53:16', '1', 'Efectivo'),
(127, 'Gaborov silvia anahi', '29220571', 'Resistencia', 'ani_gaborov@hotmail.com', 'Socios', 'Tec radiologo', '3624925733', 333914545, NULL, '2018-09-28 12:02:48', '2018-09-28 12:02:49', NULL, '2'),
(128, 'ayala jose catalino', '36632083', 'Corrientes', 'josecitoyala41@gmail.com', 'Estudiante', 'Diagnóstico por imagen', '3794698801', NULL, NULL, '2018-09-28 13:20:34', '2018-09-28 13:20:34', NULL, '1'),
(129, 'VELAZCO MARIA ALEJANDRA', '36486873', 'FONTANA', 'ale-vel09@hotmail.com', 'Profesional', 'Tec. Radiologo', '3624276444', NULL, NULL, '2018-09-28 13:53:57', '2018-11-16 14:35:03', '1', 'Efectivo'),
(130, 'GUALCO CARLOS ALBERTO', '29657871', 'FONTANA', 'ale-vel09@hotmail.com', 'Profesional', 'LIC. EN RADIOLOGIA E IMAGEN', '3624878534', NULL, NULL, '2018-09-28 13:55:49', '2018-11-16 14:35:23', '1', 'Efectivo'),
(131, 'Gonazalez Luciana', '42066973', 'puerto vilelas', 'nosotrosyoyella@gmail.com', 'Estudiante', 'Técnico Radiólogo.', '3624053072', NULL, NULL, '2018-09-28 14:34:50', '2018-09-28 14:34:50', NULL, '1'),
(132, 'Gauna Micaela sofia', '39633212', 'Corrientes', 'sofi06-05@hotmail.com', 'Estudiante', 'Técnico superior en diagnóstico por imágenes', '3786617196', 333914545, NULL, '2018-09-28 14:37:55', '2018-11-16 13:05:01', '1', 'Efectivo'),
(133, 'Romero Victoria', '38879084', 'Corrientes', 'victoria2014romero@gmail.com', 'Estudiante', 'Técnico superior en diagnóstico por imagines', '3777535854', 333914545, NULL, '2018-09-28 20:02:37', '2018-11-12 15:18:23', '1', 'Efectivo'),
(134, 'Ruiz diaz Hugo Javier', '23070787', 'Pica Roque Sáenz Peña', 'ruizdiazhugo88@gmail.com', 'Estudiante', 'Estudiante', '3644270549', NULL, NULL, '2018-09-28 20:20:10', '2018-11-23 01:10:19', '1', 'Efectivo'),
(135, 'Lanzieri Elena', '41775805', 'Corrientes', 'Mariacruzroja780@gmail.com', 'Estudiante', 'Técnico en Diagnóstico por Imagenes', '3794234314', NULL, NULL, '2018-09-28 23:39:58', '2018-11-17 10:09:16', '1', 'Efectivo'),
(136, 'Malena ksionzek', '41974190', 'Corrientes', 'malenak_2000@hotmail.com', 'Estudiante', 'Tecnico en Diagnóstico por imágenes', '3735621635', NULL, NULL, '2018-09-28 23:43:00', '2018-11-16 13:44:46', '1', 'Efectivo'),
(137, 'Valenzuela Paloma Jorgelina', '41788929', 'Corrientes', 'palojvalenzuela@gmail.com', 'Estudiante', 'Técnico en Diagnóstico por Imagen', '3773403812', NULL, NULL, '2018-09-28 23:43:35', '2018-11-22 18:48:36', '1', 'Efectivo'),
(138, 'Ledesma Florencia', '37393184', 'Corrientes', 'mafloledesma@hotmail.com', 'Estudiante', 'Técnico en Diagnóstico por Imágenes', '3794236024', NULL, NULL, '2018-09-28 23:49:25', '2018-11-17 10:07:21', '1', 'Efectivo'),
(139, 'Pineda yamila mariel', '24077840', 'Resistencia chaco', 'Sandry_goitia@hotmail.com', 'Socios al dia', 'Tecnica radiologa', '3624282846', NULL, NULL, '2018-09-29 00:57:27', '2018-11-16 13:30:42', '1', 'Efectivo'),
(140, 'Milagros Romero Espinoza', '39865268', 'Corrientes capital', 'Milirmro16@gmail.com', 'Estudiante', 'Milagros Romero Espinoza', '3794054432', NULL, NULL, '2018-09-29 01:37:42', '2018-09-29 01:37:42', NULL, '1'),
(141, 'Torres ivana cecilia', '32388822', 'Bella vista. Corrientes', 'Mamotestbv@hotmail.com', 'Profesional', 'Tecnica radiologa', '3777403733', 333914545, NULL, '2018-09-29 05:43:20', '2018-09-29 05:43:21', NULL, '2'),
(142, 'Deborah Almiron', '39519273', 'Corrientes', 'deborahalmiron11@gmail.com', 'Estudiante', 'Técnico en diagnóstico por imágenes', '3794655471', NULL, NULL, '2018-09-29 13:49:11', '2018-09-29 13:49:11', NULL, '1'),
(143, 'Andrea Leonor Ayala', '38874275', 'Corrientes', 'Andreaayala2144@gmail.com', 'Estudiante', 'Técnico en diagnóstico por imágenes', '3794905938', NULL, NULL, '2018-09-29 13:54:19', '2018-09-29 13:54:19', NULL, '1'),
(144, 'Ruda Carolina Elisabeth', '29706475', 'Saenz Peña', 'carolina23ruda@gmail.com', 'Socios', 'Tec. Radiologo', '364415571000', NULL, NULL, '2018-09-29 14:08:04', '2018-09-29 14:08:04', NULL, '1'),
(145, 'bravo victor dario', '29306190', 'Resistencia chaco', 'Sandry_goitia@hotmail.com', 'Socios al dia', 'tecnico radioogo', '3624510431', NULL, NULL, '2018-09-29 17:09:42', '2018-11-16 13:30:03', '1', 'Efectivo'),
(146, 'maidana luis alberto', '16349073', 'Resistencia chaco', 'Sandry_goitia@hotmail.com', 'Profesional', 'tecnico radioogo', '3624383800', NULL, NULL, '2018-09-29 17:11:11', '2018-11-16 13:35:31', '1', 'Efectivo'),
(147, 'marin fernando cesar', '20451175', 'Resistencia chaco', 'Sandry_goitia@hotmail.com', 'Profesional', 'Tecnico radiologo', '3624657690', NULL, NULL, '2018-09-29 17:12:20', '2018-11-16 13:26:24', '1', 'Efectivo'),
(148, 'molina teresa', '17689964', 'barranqueras', 'tere.c_33@hotmail.com', 'Profesional', 'Tecnica radiologa', '3624630309', NULL, NULL, '2018-09-29 17:13:26', '2018-11-16 13:32:30', '1', 'Efectivo'),
(149, 'cremonte silvana miriam graciela', '25904985', 'Resistencia chaco', 'silcremonte@hotmail.com', 'Profesional', 'Tecnica radiologa', '3624209439', NULL, NULL, '2018-09-29 17:14:30', '2018-11-16 13:32:08', '1', 'Efectivo'),
(150, 'toncheff veronica', '32941247', 'Resistencia chaco', 'Sandry_goitia@hotmail.com', 'Profesional', 'Tecnica radiologa', '3624315223', NULL, NULL, '2018-09-29 17:37:09', '2018-11-17 09:58:11', '1', 'Efectivo'),
(151, 'glomba gladis noemi', '25558824', 'Resistencia chaco', 'Sandry_goitia@hotmail.com', 'Profesional', 'Tecnica radiologa', '01166041981', NULL, NULL, '2018-09-29 21:01:12', '2018-11-16 13:33:37', '1', 'Efectivo'),
(155, 'Monzon Luis alejandro', '33683435', 'Corrientes', 'alejandromonzon779@gmail.com', 'Estudiante', 'Técnico superior en diagnóstico por imágenes', '3794868125', NULL, NULL, '2018-10-01 01:56:29', '2018-11-16 13:50:18', '1', 'Efectivo'),
(156, 'LESCANO FATIMA', '41070532', 'Corrientes', 'fati200872@gmail.com', 'Estudiante', 'Estudiante', '3782609930', NULL, NULL, '2018-10-01 21:16:30', '2018-11-16 13:48:38', '1', 'Efectivo'),
(157, 'Lescano juan cruz', '39565279', 'Corrientes', 'lescanojuancruz900@gmail.com', 'Estudiante', 'Estudiante', '3794486896', NULL, NULL, '2018-10-01 21:19:12', '2018-11-16 13:48:23', '1', 'Efectivo'),
(158, 'Ayala micaela antonia', '42061371', 'Corrientes', 'micaeanayala99@gmail.com', 'Estudiante', 'Estudiante', '3781410554', NULL, NULL, '2018-10-01 21:23:35', '2018-11-16 13:42:20', '1', 'Efectivo'),
(159, 'Marunak Yvan Jose Antonio', '39946367', 'Corrientes', 'Ivanmarunak21@gmail.com', 'Estudiante', 'Estudiante', '3755683242', NULL, NULL, '2018-10-01 22:01:28', '2018-11-19 12:24:41', '1', 'Efectivo'),
(160, 'Gonzalez Eduardo Sebastian', '37430375', 'Corrientes', 'edusebagon5@gmail.com', 'Estudiante', 'Tecnico superior en diagnóstico por imagen', '3795087553', NULL, NULL, '2018-10-01 22:45:48', '2018-10-01 22:45:48', NULL, '1'),
(161, 'Gonzalez Florencia Ailen', '40260562', 'Corrientes', 'Fg210924@gmail.com', 'Estudiante', 'Tecnico superior en diagnóstico por imagen', '3795087553', NULL, NULL, '2018-10-01 22:57:59', '2018-10-01 22:57:59', NULL, '1'),
(162, 'Deltin Carla', '41039964', 'Resistencia', 'Carladeltin@Outlook.com', 'Estudiante', 'Técnico superior en radiología', '3624685201', NULL, NULL, '2018-10-02 18:50:22', '2018-10-02 18:50:22', NULL, '1'),
(163, 'Silva Rosa Tamara', '39618535', 'Resistencia', 'silvatamara8796@gmail.com', 'Estudiante', 'Diagnostico Por imagen', '0362154551361', NULL, NULL, '2018-10-02 21:01:29', '2018-11-12 14:25:34', '1', 'Efectivo'),
(164, 'Oviedo Ahilen Gisella', '38872388', 'Corrientes', 'ahileoviedo11@gmail.com', 'Estudiante', 'Diagnostico Por Imagen', '03794273357', NULL, NULL, '2018-10-02 21:04:02', '2018-11-17 10:38:12', '1', 'Efectivo'),
(166, 'Romero Melina Judith', '39618885', 'Resistencia', 'rmj_18.06@outlook.com', 'Profesional', 'Técnico superior en radiología', '3624564423', 333914545, NULL, '2018-10-03 20:22:27', '2018-11-17 10:37:44', '1', 'Efectivo'),
(167, 'Ojeda leonela', '37157690', 'Corrientes', 'ojedaleonela@gmail.com', 'Profesional', 'Terciario', '03794741164', NULL, NULL, '2018-10-04 15:50:05', '2018-10-04 15:50:05', NULL, '1'),
(168, 'Cardozo Marianela Evelyn', '37975234', 'Resistencia', 'nela_cardozo@hotmail.com', 'Profesional', 'Técnico superior en radiología', '3624300101', 333914545, NULL, '2018-10-04 16:58:32', '2018-11-16 13:04:38', '1', 'Efectivo'),
(169, 'Serra sofia elisabeth', '36017867', 'Resistencia', 'Serrasofia3625@gmail.com', 'Estudiante', 'Tecnicatura en radiolagia', '3625234176', NULL, NULL, '2018-10-04 23:58:52', '2018-10-04 23:58:52', NULL, '1'),
(170, 'Birene Martinez Feliz Cesar', '26893785', 'Libertador general san martin', 'fcbirene@gmail.com', 'Profesional', 'Lic. En Radiologia e Imágenes', '03886432017', NULL, NULL, '2018-10-05 12:36:44', '2018-10-05 12:36:44', NULL, '1'),
(171, 'Aquino Sabas Waldemar', '16750314', 'Libertador general san martin', 'fcbirene@gmail.com', 'Profesional', 'Lic. En Radiologia e Imágenes', '3884619511', NULL, NULL, '2018-10-05 12:39:23', '2018-10-05 12:39:23', NULL, '1'),
(172, 'Di Natale Jeremias', '35370634', 'Corrientes', 'jeredinatale@outlook.com', 'Estudiante', 'técnico superior en diagnóstico por imágenes', '3794872410', NULL, NULL, '2018-10-05 22:21:48', '2018-10-05 22:21:48', NULL, '1'),
(173, 'Maidana Elios Rafael', '40049278', 'Corrientes, Capital', 'eliomaidana96@hotmail.com', 'Estudiante', 'Tecnico Superior en diagnostico por imagenes', '3794239606', NULL, NULL, '2018-10-06 12:21:58', '2018-11-20 12:35:03', '1', 'Efectivo'),
(174, 'Rios walter Jose', '34654930', 'Corrientes Capital', 'Joserios_w@hotmail.com', 'Profesional', 'Técnico Superior En Diagnóstico por Imagen', '3794358994', NULL, NULL, '2018-10-06 22:23:57', '2018-11-17 10:36:45', '1', 'Efectivo'),
(175, 'Orquiola Laura Romina', '37392681', 'Corrientes', 'romy_lau@hotmail.es', 'Profesional', 'Tecnico Superior en Diagnostico por Imagen', '3794357968', NULL, NULL, '2018-10-08 13:11:05', '2018-11-16 14:43:48', '1', 'Efectivo'),
(176, 'MATIJASEVICH SERGIO DANIEL', '23263665', 'BARRAQUERAS', 'patomatija@hotmail.com', 'Profesional', 'LICENCIADO E RADIOLOGIA', '3624226966', NULL, NULL, '2018-10-08 16:39:36', '2018-11-16 14:43:24', '1', 'Efectivo'),
(177, 'gonzalez tamara del mar', '40700998', 'empedrado- corrientes', 'marr_999@outlook.es', 'Estudiante', 'Tecnico superior en diagnostico por imagen', '3794351232', NULL, NULL, '2018-10-09 00:45:50', '2018-10-09 00:45:50', NULL, '1'),
(178, 'Fonteina Claudia carolina', '30996546', 'Resistencia', 'Claudiafonteina84@gmail.com', 'Estudiante', 'No', '3624722121', NULL, NULL, '2018-10-09 14:23:06', '2018-11-19 12:43:41', '1', 'Efectivo'),
(179, 'Chapero Sofia', '38816115', 'Corrientes', 'schapero95@hotmail.com', 'Estudiante', 'Técnico superior en diagnóstico por imagines', '3482588653', NULL, NULL, '2018-10-09 20:39:56', '2018-11-12 14:39:41', '1', 'Efectivo'),
(180, 'Rivero Milena', '37077207', 'Resistencia', 'myle2012@hotmail.com.ar', 'Estudiante', 'Técnico radiólogo', '3625264497', NULL, NULL, '2018-10-09 21:15:59', '2018-11-14 12:31:24', '1', 'Efectivo'),
(181, 'Almiron Maria', '41635745', 'Corrientes', 'Maar.almiiroon@gmail.com', 'Estudiante', 'Técnico radiologo', '3794753575', NULL, NULL, '2018-10-09 21:16:59', '2018-11-14 12:33:51', '1', 'Efectivo'),
(182, 'Rivero Milena', '37077206', 'Resistencia', 'myle2012@hotmail.com.ar', 'Estudiante', 'Técnico radiólogo', '3625264497', NULL, NULL, '2018-10-09 21:19:20', '2018-11-17 10:34:34', '1', 'Efectivo'),
(183, 'Romero Miguel Osvaldo Sebastian', '37886702', 'Corrientes Capital', 'sebasromero.886@gmail.com', 'Estudiante', 'Estudiante', '3794920918', NULL, NULL, '2018-10-09 23:18:41', '2018-11-16 13:52:33', '1', 'Efectivo'),
(184, 'Ugolini Esther Del Pilar', '20939339', 'corrientes', 'estherugo12@gmail.com', 'Estudiante', 'tecnico radiologo', '3794884474', NULL, NULL, '2018-10-10 12:13:17', '2018-11-12 15:20:50', '1', 'Efectivo'),
(185, 'Vallejos Yolanda Noemi', '33636416', 'corrientes', 'yolyvallejos47@gmail.com', 'Estudiante', 'tecnico radiologo', '3794562424', NULL, NULL, '2018-10-10 12:16:27', '2018-11-12 15:17:25', '1', 'Efectivo'),
(186, 'flores acosta paula pierina', '33792595', 'corrientes', 'erina_19_14@hotmail.com', 'Estudiante', 'tecnico radiologo', '3794688288', 333914545, NULL, '2018-10-10 12:18:08', '2018-11-16 13:04:11', '1', 'Efectivo'),
(187, 'Schuster Cecilia Ines', '32183432', 'Corrientes', 'ceciliaschuster@hotmail.com', 'Estudiante', 'Tecnico Radiologo', '3794890784', NULL, NULL, '2018-10-10 12:19:49', '2018-10-10 12:19:49', NULL, '1'),
(188, 'Cuellar ramon nelson', '29499269', 'Salta', 'cuellar29499269@gmail.com', 'Estudiante', 'Tecnico superior en radiologia', '3878326441', 333914545, NULL, '2018-10-10 20:01:52', '2018-11-16 13:03:50', '1', 'Efectivo'),
(189, 'Posse Selva Mariela', '35678436', 'Formosa', 'marielaposse90@hotmail.com', 'Estudiante', 'Estudiante de Radiologia', '2613004815', NULL, NULL, '2018-10-10 20:08:00', '2018-10-10 20:08:00', NULL, '1'),
(190, 'Lopez Pedro matias', '32836731', 'Corrientes', 'pedromatiaslopez87@gmail.com', 'Estudiante', 'Técnico superior en diagnóstico por imágenes', '0379154013165', 333914545, NULL, '2018-10-10 20:24:23', '2018-11-16 13:03:29', '1', 'Efectivo'),
(191, 'Torres Emilia', '40279950', 'Corrientes', 'emilia.e.torres@gmail.com', 'Estudiante', 'Estudiante', '3794908350', 333914545, NULL, '2018-10-10 22:59:22', '2018-11-16 13:03:09', '1', 'Efectivo'),
(192, 'Maidana tiji', '40501320', 'Resistencia', 'tijimarisol@yahoo.com.ar', 'Estudiante', 'Tecnico radiología', '3624659463', NULL, NULL, '2018-10-11 00:23:29', '2018-11-16 13:47:55', '1', 'Efectivo'),
(193, 'Gabriela zayas', '41229118', 'Formosa', 'Gabrielazayas81@gmail.com', 'Estudiante', 'Estudiante', '3704079034', NULL, NULL, '2018-10-11 01:23:20', '2018-10-11 01:23:20', NULL, '1'),
(194, 'Struciat Oscar', '29019346', 'basail', 'coas699@hotmail.com', 'Organizador', 'licenciado en Radiologia', '3624593997', 333914545, NULL, '2018-10-11 01:39:35', '2018-11-16 20:35:15', '1', 'Efectivo'),
(195, 'Noguera Nadia Soledad', '40048717', 'Corrientes', 'nadianoguera7@gmail.com', 'Estudiante', 'Tec. En Diagnostico por Imagenes', '3794983885', NULL, NULL, '2018-10-11 14:22:35', '2018-10-11 14:22:35', NULL, '1'),
(196, 'Aguirre Lezcano Aymara Estefania', '39778961', 'Corrientes', 'aymaraguirre7@gmail.com', 'Estudiante', 'Estudiante', '3794250171', NULL, NULL, '2018-10-11 17:31:10', '2018-10-11 17:31:10', NULL, '1'),
(197, 'Encina Daiana Marlene', '39863222', 'Corrientes', 'encinamarlene844@gmail.com', 'Estudiante', 'Estudiante', '3794699013', NULL, NULL, '2018-10-11 17:38:32', '2018-10-11 17:38:32', NULL, '1'),
(198, 'Pereyra Almiron Belen Liliana', '40068192', 'Corrientes', 'Pequepupi25@gmail.com', 'Estudiante', 'Estudiante', '3795021225', NULL, NULL, '2018-10-11 17:45:03', '2018-10-11 17:45:03', NULL, '1'),
(199, 'Casimiro jose oscar', '37418990', 'Salta', 'jose_fejix-123@hotmail.com', 'Estudiante', 'Tecnico superior en radiologia', '3874538351', 333914545, NULL, '2018-10-11 19:53:53', '2018-10-11 19:53:54', NULL, '2'),
(200, 'Cuevas Leonardo Oscar', '38190147', 'Corrientes', 'leoocuevas@gmail.com', 'Estudiante', 'Técnico Superior en Diagnóstico por Imágenes', '3794670163', NULL, NULL, '2018-10-11 21:47:01', '2018-11-16 13:52:53', '1', 'Efectivo'),
(202, 'Popolizio Garcia Gabriel Alejandro', '22938186', 'Charata -Chaco', 'gabriel.popolizio79@gmail.com', 'Profesional', 'Técnico Superior en Diagnostico por Imágenes', '5403731546438', 333914545, NULL, '2018-10-12 15:37:28', '2018-11-16 13:02:40', '1', 'Efectivo'),
(203, 'Taboada Sandra Julieta', '21546093', 'San Salvador de Jujuy', 'Taboadasandra099@gmail.com.ar', 'Socios al dia', 'Licenciada', '3884097094', NULL, NULL, '2018-10-17 04:35:46', '2018-11-17 11:12:59', '1', 'Efectivo'),
(204, 'Jose genaro Rojas', '24816638', 'Libertador Gral San Martin', 'rojasjosegenaro@gmail.com', 'Profesional', 'Licenciado', '15414398', NULL, NULL, '2018-10-17 19:53:55', '2018-10-17 19:53:55', NULL, '1'),
(205, 'moscoso pedraza viviana', '92929610', 'cordoba', 'vivianaruthmoscoso63@gmail.com', 'Profesional', 'licenciada en producciom de bioimagenes', '03583431860', NULL, NULL, '2018-10-17 21:20:17', '2018-11-14 12:20:05', '1', 'Efectivo'),
(206, 'Moreno Agustina', '36204394', 'Formosa', 'Agushmoreno@hotmail.com', 'Estudiante', 'Técnico en radiologia', '3716616620', NULL, NULL, '2018-10-17 23:45:11', '2018-11-12 15:50:57', '1', 'Efectivo'),
(207, 'Trinidad Paola Gabriela', '39318773', 'Las lomitas -Formosa', 'Paolatrinidad106@Gmail.com', 'Estudiante', 'Técnico Radiologo', '3704560409', NULL, NULL, '2018-10-18 00:53:48', '2018-11-12 15:51:30', '1', 'Efectivo'),
(208, 'Palomo Natalia yanina', '42756893', 'Las lomitas', 'natalia.palomo.05@gmail.com', 'Estudiante', 'Técnico radiologo', '3715486358', NULL, NULL, '2018-10-18 00:55:06', '2018-11-12 15:52:08', '1', 'Efectivo'),
(213, 'Cerullo Lucia Angelina', '40124126', 'Corrientes', 'Luciaangelina96@gmail.com', 'Estudiante', 'Estudiante', '3795055228', NULL, NULL, '2018-10-18 17:15:22', '2018-11-12 15:52:43', '1', 'Efectivo'),
(218, 'Torrico Samanta Anahi', '27011802', 'San Salvador de Jujuy', 'anahitorrico@yahoo.co.ar', 'Profesional', 'Licenciada', '3885475612', NULL, NULL, '2018-10-18 21:33:09', '2018-10-18 21:33:09', NULL, '1'),
(219, 'Romero Laura Jaquelina', '28684680', 'Mendoza', 'rlaurajaquelina@yahoo.com.ar', 'Profesional', 'Licenciada en Produccion de Bio Imagen', '0262215587834', NULL, NULL, '2018-10-18 21:34:51', '2018-11-12 14:32:03', '1', 'Efectivo'),
(220, 'Rolon Sonia Hermelinda', '25518523', 'Barranqueras - Chaco', 'sokinharolon@gmail.com', 'Socios al dia', 'Licenciada en Radiología e Imágenes', '3624774513', 333914545, NULL, '2018-10-19 12:28:27', '2018-11-16 13:02:03', '1', 'Efectivo'),
(221, 'Orellana Franco Damian', '33377036', 'Las Breñas', 'Francoorellana54@gmail.com', 'Estudiante', 'Ninguno', '3731555403', 333914545, NULL, '2018-10-19 14:30:04', '2018-11-16 13:00:33', '1', 'Efectivo'),
(222, 'Aloi Sebastian', '32871479', 'Resistencia', 'seba_losredo@hotmail.com', 'Profesional', 'Técnico Superior en Diagnóstico por Imágenes y Terapia Radiante', '3624727020', 333914545, NULL, '2018-10-19 15:38:18', '2018-11-16 13:00:13', '1', 'Efectivo'),
(223, 'Parente Emanuel', '37575113', 'Santa Fe', 'emanuel.parente@diagmedico.com', 'Disertante', 'Técnico en diagnostico por imágenes', '0374615618551', NULL, NULL, '2018-10-19 15:54:15', '2018-11-16 18:52:59', '1', 'Efectivo'),
(224, 'CAZETTI MARIA ELENA', '23635165', 'RESISTENCIA', 'marisolca1973@outlook.es', 'Profesional', 'TECNICO RADIOLOGO', '3624720667', 333914545, NULL, '2018-10-19 23:14:32', '2018-10-19 23:14:33', NULL, '2'),
(225, 'Sanchez Juan Carlos', '23247848', 'Resistencia', 'jcsanchezlic3@gmail.com', 'Organizador', 'Licenciado en Radiologia e imagen', '3624364699', 333914545, NULL, '2018-10-20 02:22:05', '2018-11-09 19:50:09', '1', 'Efectivo'),
(226, 'Perez Orellana Maria de los Milagros', '34445852', 'Corrientes capital', 'mili.perezorellana@gmail.com', 'Estudiante', 'Tecnico Superior en Diagnostico por Imagenes', '3794976885', NULL, NULL, '2018-10-20 19:30:01', '2018-10-20 19:30:01', NULL, '1'),
(227, 'Maidana Ramon Antonio', '14289131', 'Resistencia', 'yanetmaidana21@gmail.com', 'Estudiante', 'Estudiante', '3624607729', 333914545, NULL, '2018-10-22 14:59:29', '2018-11-17 10:43:49', '1', 'Efectivo'),
(230, 'Romero Christian Gerardo', '30391242', 'Chaco', 'christian_tigre22@hotmail.com', 'Profesional', 'Técnico en Radiología', '3624017240', NULL, NULL, '2018-10-22 16:07:52', '2018-11-16 14:45:36', '1', 'Efectivo'),
(231, 'ALVARENGA MARTIN', '31457860', 'POSADAS', 'alvarenga_martin@hotmail.com', 'Profesional', 'TECNICO RADIOLOGO', '3764330613', 333914545, NULL, '2018-10-22 22:22:41', '2018-11-17 10:43:19', '1', 'Efectivo'),
(232, 'TABACCHI HECTOR IVAN', '26874870', 'POSADAS', 'jetrira@gmail.com', 'Profesional', 'TECNICO RADIOLOGO', '3764710999', 333914545, NULL, '2018-10-22 22:28:10', '2018-11-17 10:42:47', '1', 'Efectivo'),
(233, 'Gomez Borelli Rodrigo', '22711273', 'Resistencia', 'kari_liv@hotmail.com', 'Profesional', 'Tec. Radiologo', '3624066144', NULL, NULL, '2018-10-22 23:10:03', '2018-11-12 15:32:52', '1', 'Efectivo'),
(234, 'Nada antonella elizabeth', '38123573', 'Las breñas chaco', 'Anto_nalda@hotmail.com', 'Estudiante', 'Estudiante', '3731648511', 333914545, NULL, '2018-10-23 00:03:45', '2018-11-17 10:42:15', '1', 'Efectivo'),
(235, 'Bruno Natalia Elisabeth', '31325002', 'Resistencia', 'nataliaelisabethbruno@gmail.com', 'Profesional', 'Técnico Radiologo', '3624244321', NULL, NULL, '2018-10-23 00:49:23', '2018-11-17 10:41:54', '1', 'Efectivo'),
(236, 'Zampar Gustavo Antonio', '17689704', 'Resistencia-Chaco', 'gustavo_zampar@hotmail.com', 'Profesional', 'Técnico Radiologo', '3624778082', NULL, NULL, '2018-10-23 01:48:32', '2018-11-19 11:49:43', '0', 'Efectivo'),
(237, 'Zampar Mauro Ezequiel', '35037313', 'Resistencia-Chaco', 'mauro_zampar@outlook.es', 'Estudiante', 'Técnico Radiologo', '3624020314', NULL, NULL, '2018-10-23 01:50:31', '2018-11-12 15:55:47', '1', 'Efectivo'),
(238, 'Natalia Soledad Gonzalez', '33072486', 'Resistencia', 'natunet198@hotmail.com', 'Estudiante', 'Tec. Radiología. Especialista en TAC.', '3624873187', 333914545, NULL, '2018-10-23 13:34:40', '2018-10-23 13:34:41', NULL, '2'),
(239, 'Yaya Ana Paula', '36116184', 'Resistencia', 'yayaanapaula@gmail.com', 'Profesional', 'Tecnico Radiologo', '3624641914', 333914545, NULL, '2018-10-23 13:41:45', '2018-10-23 13:41:46', NULL, '2'),
(240, 'FERNANDEZ DANIEL ALFREDO', '21366335', 'Villa Berthet', 'corrientes_daf@outlook.com', 'Profesional', 'Tecnico Superior en Radiologia', '3735535206', 333914545, NULL, '2018-10-23 13:56:25', '2018-10-23 13:56:26', NULL, '2'),
(243, 'Vargas Samuel Matias', '33724349', 'Resistencia', 'samuelmatiasvargas@gmail.com', 'Socios al dia', 'Tec. Radiologo', '3624546668', NULL, NULL, '2018-10-24 00:02:31', '2018-11-17 10:41:13', '1', 'Efectivo'),
(244, 'ZDANOWICZ MARINO GABRIEL', '31003985', 'Corrientes', 'Marinozdanowicz@gmail.com', 'Profesional', 'Licenciado en Bioimagenes', '3794728967', NULL, NULL, '2018-10-24 03:19:20', '2018-11-17 10:40:51', '1', 'Efectivo'),
(245, 'Motovski Rosana karina', '25689395', 'P.Roque Saenz Peña', 'rmotovski@gmail.com.ar', 'Profesional', 'Técnica Radiologa', '3644311831', NULL, NULL, '2018-10-24 11:42:38', '2018-10-24 11:42:38', NULL, '1'),
(246, 'Gonzalez Esquivel  Gustavo Fabian', '26396212', 'Corrientes', 'gutygonzes@hotmail.com', 'Estudiante', 'tecnico radiologo', '3794565825', 333914545, NULL, '2018-10-24 11:59:15', '2018-11-16 12:53:45', '1', 'Efectivo'),
(247, 'Romero Eve Dahiana', '38875263', 'Corrientes', 'Dahianarv@outlook.com', 'Estudiante', 'Estudiante', '3777567924', NULL, NULL, '2018-10-24 14:30:39', '2018-11-12 15:54:23', '1', 'Efectivo'),
(248, 'Paredes Priscila', '37169527', 'Resistencia', 'priscii_paredes@hotmail.com', 'Estudiante', 'Técnico radiologo', '3625248134', NULL, NULL, '2018-10-24 21:22:06', '2018-10-24 21:22:06', NULL, '1'),
(249, 'Maldonado Andrea Carolina', '38876952', 'Corrientes Capital', 'maldonadoandrea016@gmail.com', 'Estudiante', 'Técnico Superior en Diagnostico por Imágenes', '3794882914', NULL, NULL, '2018-10-25 00:39:56', '2018-11-17 10:39:43', '1', 'Efectivo'),
(250, 'Sanchez Rodrigo', '39615387', 'Barranqueras', 'sanchezrodrigo7.rs@gmail.com', 'Estudiante', '-', '3624016230', NULL, NULL, '2018-10-25 05:24:48', '2018-10-25 05:24:48', NULL, '1'),
(251, 'Holenich Belen', '41335226', 'Corrientes', 'holenichh@gmail.com', 'Estudiante', 'Técnico superior en diagnóstico por imágenes', '03794406219', NULL, NULL, '2018-10-25 14:02:47', '2018-11-17 10:39:21', '1', 'Efectivo'),
(252, 'Duarte Angeles Helen', '39177196', 'Resistencia', 'anggie.d@hotmail.com', 'Estudiante', 'Técnico superior en Radiología', '3624778200', NULL, NULL, '2018-10-25 14:06:28', '2018-10-25 14:06:28', NULL, '1'),
(253, 'Troxler Juan Manuel', '32728973', 'Resistencia', 'juanmtroxler@hotmail.com.ar', 'Estudiante', 'Tecnico superor en radiologia', '3624158842', NULL, NULL, '2018-10-25 14:10:10', '2018-10-25 14:10:10', NULL, '1'),
(254, 'hector eduardo conforti', '20587630', 'Resistencia chaco', 'Hecrorconfortii@gmail.com', 'Profesional', 'Tecnico radiologo', '3624282846', NULL, NULL, '2018-10-25 14:34:19', '2018-11-16 15:35:20', '1', 'Efectivo'),
(258, 'Jorge Monica', '26570570', 'SANTIAGO DEL ESTERO', 'moni_jo78@hotmail.com', 'Profesional', 'TECNICA RADIOLOGA', '385154895289', NULL, NULL, '2018-10-25 23:39:56', '2018-11-19 11:42:06', '1', 'Efectivo'),
(259, 'garcia julio Ismael', '28661564', 'Resistencia', 'juliorx22@gmail.com', 'Profesional', 'Técnico Radiológo', '3624160634', NULL, NULL, '2018-10-26 02:09:38', '2018-11-16 13:50:40', '1', 'Efectivo'),
(260, 'Amarilla Teresa Leandra', '30964123', 'Corrientes', 'amarillateresaleandra@gmail.com', 'Profesional', 'Tecnico superior en Diagnostico por Imagenes', '3794545776', 333914545, NULL, '2018-10-26 12:05:21', '2018-11-12 15:39:53', '1', 'Efectivo'),
(261, 'Cardenas Rodrigo', '30712192', 'santiago del estero', 'ersombras@gmail.com', 'Profesional', 'Licenciado en Diagnostico por Imagenes', '385154838132', NULL, NULL, '2018-10-26 13:22:08', '2018-11-20 19:19:27', '1', 'Efectivo'),
(262, 'Pegoraro Liliana Inés', '23014634', 'Miraflores Chaco', 'lilipeg@hotmail.com', 'Socios al dia', 'Licenciado en Radiologia e imagen', '3734403805', NULL, NULL, '2018-10-26 14:06:22', '2018-11-19 11:24:19', '1', 'Efectivo'),
(263, 'Nuñez Natalia Ely', '33224224', 'Formosa', 'naty22061987@gmail.com', 'Profesional', 'Tec. Radiologo', '3704002525', NULL, NULL, '2018-10-26 14:09:53', '2018-11-16 13:21:48', '1', 'Efectivo'),
(264, 'Montilla Cecilia Verónica', '27400199', 'Barranqueras', 'Ceciliamontilla@hotmail.com', 'Profesional', 'Tec radiologo', '3624011294', NULL, NULL, '2018-10-26 14:11:15', '2018-10-26 14:11:15', NULL, '1'),
(265, 'Chamorro Juan', '32375432', 'Resistencia', 'Maritavillalba6@gmail.com', 'Estudiante', 'Técnico radiologo', '3624624258', NULL, NULL, '2018-10-26 14:45:19', '2018-10-26 14:45:19', NULL, '1'),
(266, 'Valenzuela Verònica Giselle', '34413970', 'Resistencia', 'Gigivalenzuela89@gmail.com', 'Profesional', 'Tec. Radiologo', '3624160632', NULL, NULL, '2018-10-26 20:28:56', '2018-11-19 11:23:30', '1', 'Efectivo'),
(267, 'Juan Facundo Uferer Ferreyra', '31119278', 'Resistencia', 'juanfacundouf@gmail.com', 'Organizador', 'Universitario', '03624273769', NULL, NULL, '2018-10-26 23:23:18', '2018-11-12 13:20:18', '1', 'Efectivo'),
(268, 'Peña Gonzalo', '38768712', 'Barranqueras', 'Gonza_adrian@outlook.com', 'Estudiante', '-', '3624684211', NULL, NULL, '2018-10-27 04:43:29', '2018-10-27 04:43:29', NULL, '1'),
(269, 'MENESES BUENDIA, NICOLE', '39634353', 'Corrientes capital', 'nicole30.meneses@gmail.com', 'Estudiante', 'Tecnico Superior en Diagnostico por Imagenes', '3795002005', NULL, NULL, '2018-10-27 12:31:38', '2018-10-27 12:31:38', NULL, '1'),
(270, 'Guaymas Sabina Viviana', '20352303', 'Salta', 'vivisabi016@gmail.com', 'Profesional', 'Licenciado en Radiologia', '3874411187', NULL, NULL, '2018-10-27 15:32:30', '2018-10-27 15:32:30', NULL, '1'),
(271, 'Almaraz Vanesa Marisel', '32414129', 'Santiago Del Estero', 'vanesa-almaraz@hotmail.com', 'Profesional', 'Lic. En Produccion de Bioimagenes', '3854853656', NULL, NULL, '2018-10-29 01:20:42', '2018-10-29 01:20:42', NULL, '1'),
(272, 'Sosa Reyes Marina Belen', '36673611', 'Corrientes', 'srmb010392@gmail.com', 'Profesional', 'Técnico en Diagnostico por Imágenes', '3794829052', NULL, NULL, '2018-10-29 12:39:04', '2018-11-12 16:01:33', '1', 'Efectivo'),
(273, 'Sian Bárbara antonella', '40210828', 'Formosa', 'Barby_sian@hotmail.com', 'Estudiante', 'Estudiante', '3704043046', 333914545, NULL, '2018-10-29 15:32:22', '2018-11-16 12:52:08', '1', 'Efectivo'),
(274, 'Núñez Yamila', '40084956', 'Formosa, capital', 'yamila-28@outlook.com', 'Estudiante', 'Estudiante', '3705032174', 333914545, NULL, '2018-10-29 15:47:00', '2018-11-16 12:50:19', '1', 'Efectivo'),
(275, 'BARRIOS CARLOS JAVIER', '37478342', 'Gral. San martin ( CHACO )', 'Xavibarrios_93@hotmail.com', 'Profesional', 'TECNICO RADIOLOGO', '3725407201', NULL, NULL, '2018-10-29 18:02:17', '2018-11-12 14:48:51', '1', 'Efectivo');
INSERT INTO `inscriptos` (`id`, `nya`, `dni`, `ciudad`, `email`, `caracter`, `titulo`, `contacto`, `ml_id`, `curso_id`, `created_at`, `updated_at`, `pago`, `metodo_pago`) VALUES
(276, 'Valtier María de los Ángeles', '27662649', 'Resistencia', 'mariangelesvalti@hotmail.com', 'Estudiante', 'Técnico radiologo', '3624786913', 333914545, NULL, '2018-10-29 20:04:44', '2018-11-16 12:51:43', '1', 'Efectivo'),
(277, 'Suave Joaquin Ezequiel', '40876831', 'Corrientes', 'joasuave1@gmail.com', 'Estudiante', 'estudiante', '3795017423', NULL, NULL, '2018-10-29 23:31:06', '2018-11-12 14:27:58', '1', 'Efectivo'),
(278, 'Jaguer Carla natalia', '42707261', 'San Ramon de la nueva Oran', 'Carla_15_jaguer@hotmail.com', 'Estudiante', 'Tecnico Radiologo', '3878541155', NULL, NULL, '2018-10-30 14:59:10', '2018-10-30 14:59:10', NULL, '1'),
(279, 'Cesar Gadea', '30245451', 'Mercedes', 'cesargadea@hotmail.com', 'Profesional', 'Técnico radiólogo', '3773410312', 333914545, NULL, '2018-10-31 12:15:00', '2018-11-16 12:32:40', '1', 'Efectivo'),
(280, 'Gomez Karina Beatriz', '23795531', 'Resistencia - Chaco', 'karybenja16@gmail.com', 'Profesional', 'Tecnica Radióloga', '3624790688', 333914545, NULL, '2018-10-31 13:02:14', '2018-10-31 13:02:14', NULL, '2'),
(281, 'Soto Santana Sergio fabian', '29137454', 'Resistencia', 'sergio_rx81@hotmail.com', 'Profesional', 'Tecnico Radiologo', '3624048664', NULL, NULL, '2018-11-02 22:12:40', '2018-11-17 10:50:31', '1', 'Efectivo'),
(282, 'Medina Santiago', '37708146', 'Resistencia', 'santiago.medina1819@yahoo.com.ar', 'Estudiante', 'tecnicatura en radiologia', '3624014382', NULL, NULL, '2018-11-05 21:49:13', '2018-11-05 21:49:13', NULL, '1'),
(283, 'Schulz Mariela', '30484090', 'Basail', 'mariela_schulz@yahoo.com.ar', 'Socios al dia', 'Licenciada en Producción de Bioimágenes', '3624757162', NULL, NULL, '2018-11-05 22:17:14', '2018-11-12 14:36:10', '1', 'Efectivo'),
(284, 'LARA CELIA RAQUEL', '16897836', 'RESISTENCIA', 'celiarlara@gmail.com', 'Socios al dia', 'Licenciada', '3624139520', NULL, NULL, '2018-11-05 22:19:41', '2018-11-12 16:02:27', '1', 'Efectivo'),
(285, 'Echezarreta Rodrigo Luis', '37703862', 'Resistencia', 'rodrigo.echezarreta7@gmail.com', 'Profesional', 'Técnico Superior', '3624276276', 333914545, NULL, '2018-11-05 22:21:17', '2018-11-16 12:27:37', '1', 'Efectivo'),
(286, 'Berger Andrea', '27410255', 'Resistencia', 'bergerandrea79@gmail.com', 'Socios al dia', 'Técnica Radiologa', '3794816796', NULL, NULL, '2018-11-05 22:24:53', '2018-11-12 15:33:20', '1', 'Efectivo'),
(287, 'Pazzaglia Rosina', '39366213', 'Rosario, Santa Fe', 'rosinapazzaglia95@hotmail.com', 'Estudiante', 'Ninguno', '0346015696476', NULL, NULL, '2018-11-06 00:19:31', '2018-11-12 15:50:27', '1', 'Efectivo'),
(288, 'Zalazar cinthya romina', '37206518', 'Corrientes', 'cinthyarominaz@gmail.com', 'Profesional', 'Tecnica superior en diagnostico por imagenes', '3794686192', NULL, NULL, '2018-11-06 00:25:37', '2018-11-12 14:35:20', '1', 'Efectivo'),
(289, 'Barbieri Mónica Patricia', '22002301', 'Resistencia', 'Jjm_70@hotmail.com', 'Profesional', 'Licenciada en Radiología e Imágenes', '3624709761', 333914545, NULL, '2018-11-06 01:59:03', '2018-11-17 10:47:37', '1', 'Efectivo'),
(290, 'Ferreira Fátima Anabella', '33404765', 'Resistencia', 'faty_8722@hotmail.com', 'Profesional', 'Técnica Radiologa', '1133224571', NULL, NULL, '2018-11-06 10:06:44', '2018-11-16 14:44:15', '1', 'Efectivo'),
(291, 'Florenciano, Carlos David', '34718717', 'Corrientes', 'carlosflorenciano10@gmail.com', 'Profesional', 'Técnico en Diagnóstico por Imágenes', '3794892507', NULL, NULL, '2018-11-06 16:02:30', '2018-11-12 14:48:23', '1', 'Efectivo'),
(292, 'Romero Eliana Elizabeth', '38967457', 'Barranqueras', 'eliana.romero2@gmail.com', 'Profesional', 'Técnica Superior en Diagnóstico por Imágenes', '3624605434', NULL, NULL, '2018-11-06 18:33:56', '2018-11-12 14:46:24', '1', 'Efectivo'),
(293, 'Suárez Mariela Alejabdra', '28090027', 'Corrientes', 'mary3-4@outlook.es', 'Profesional', 'Técnica Radiologa', '3794869157', 333914545, NULL, '2018-11-07 13:18:41', '2018-11-16 12:26:11', '1', 'Efectivo'),
(294, 'romero cinthia vanesa', '29554730', 'resistencia', 'cinthiavanesaromero@gmail.com', 'Socios al dia', 'licenciada en produccion de bioimagenes', '3624334202', 333914545, NULL, '2018-11-07 13:42:25', '2018-11-12 15:57:44', '1', 'Efectivo'),
(295, 'Ocampo Llano Natasha Tamara', '36625140', 'Corrientes', 'natashas@live.com.ar', 'Estudiante', 'Tecnico superior en diagnostico por imagen', '3784402061', NULL, NULL, '2018-11-07 13:42:37', '2018-11-07 13:42:37', NULL, '1'),
(296, 'romero cinthia vanesa', '2955470', 'resistencia', 'cinthiavanesaromero@gmail.com', 'Profesional', 'licenciada en produccion de bioimagenes', '3624334202', NULL, NULL, '2018-11-07 13:54:39', '2018-11-17 10:46:26', '1', 'Efectivo'),
(297, 'Analia Ofelia Veron', '27991904', 'Resitencia', 'anaoferver@gmail.com', 'Profesional', 'Tecnica Radiologa', '3624610383', 333914545, NULL, '2018-11-07 15:30:17', '2018-11-14 12:34:55', '1', 'Efectivo'),
(298, 'Rodríguez Victor antonio', '36970332', 'Resistencia', 'rodriguez-toni@hotmail.com', 'Estudiante', 'Técnico superior en radiologia', '3731624031', NULL, NULL, '2018-11-07 15:57:27', '2018-11-07 15:57:27', NULL, '1'),
(299, 'Requejo Paula Noemi', '26658280', 'Resistencia', 'paulaa_angel@hotmail.com', 'Organizador', 'Secundario', '3624222013', NULL, NULL, '2018-11-07 17:26:31', '2018-11-16 13:25:36', '1', 'Efectivo'),
(300, 'Fernandez Dario Alejandro', '36972451', 'Resistencia', 'alejandrofernandez.ADF@gmail.com', 'Profesional', 'Técnico Superior en Radiología', '3624624825', NULL, NULL, '2018-11-07 18:44:36', '2018-11-17 11:20:59', '1', 'Efectivo'),
(301, 'valenzuela victor fabian', '31627389', 'barranqueras', 'ellocofabi@gmail.com', 'Profesional', 'tecnico superior en diagnostico por imagenes', '3624713324', 333914545, NULL, '2018-11-07 20:47:39', '2018-11-17 11:20:40', '1', 'Efectivo'),
(302, 'Borda Mirian Leonor', '22164458', 'Resistencia', 'milylb27@hotmail.com.ar', 'Profesional', 'Tec. Radiologo', '3624789696', NULL, NULL, '2018-11-07 21:52:47', '2018-11-12 16:01:01', '1', 'Efectivo'),
(303, 'Ramirez Florencia Belen', '39900687', 'Corrientes', 'florenciaramirezz87@gmail.com', 'Estudiante', 'Estudiante', '3704098498', 333914545, NULL, '2018-11-07 23:25:30', '2018-11-16 12:24:45', '1', 'Efectivo'),
(304, 'Brest Eliana', '32388913', 'Resistencia', 'Elianabrest86@outlook.com', 'Profesional', 'Tec superior en diagnóstico por imágenes', '3625160664', NULL, NULL, '2018-11-07 23:28:25', '2018-11-07 23:28:25', NULL, '1'),
(305, 'Del toso Giovanna caterina', '32210258', 'Resistencia', 'Giovannadeltos@gmail.com', 'Profesional', 'Licenciada en producción de bioinagenes', '36247887626', NULL, NULL, '2018-11-08 02:36:52', '2018-11-12 15:56:18', '1', 'Efectivo'),
(306, 'Gomez Maira', '35689869', 'Resistencia', 'mairagomez1964@gmail.com', 'Socios al dia', 'Tecnico superior en radiologia', '3644562457', NULL, NULL, '2018-11-08 03:03:53', '2018-11-17 09:49:46', '1', 'Efectivo'),
(307, 'Aucar Beneventano Andrea J.', '36989739', 'Corrientes', 'Andrux0365@gmail.com', 'Profesional', 'Técnico superior en Diagnostico por Imágenes', '3794829938', NULL, NULL, '2018-11-08 03:18:44', '2018-11-12 15:00:59', '1', 'Efectivo'),
(308, 'menna carina alejandra', '23136139', 'resistencia', 'carinaalejandramenna@gmail.com', 'Profesional', 'tecnica radiologa', '3624227772', 333914545, NULL, '2018-11-08 14:30:19', '2018-11-08 14:30:20', NULL, '2'),
(309, 'Fernandez Milagros Liset', '37883927', 'Corrientes', 'miile_23@hotmail.com', 'Profesional', 'Técnico superior en Diagnostico por Imágenes', '3794203460', NULL, NULL, '2018-11-08 15:00:40', '2018-11-12 15:01:33', '1', 'Efectivo'),
(310, 'Losito Claudia Victoria', '18620628', 'Resistencia', 'claudialosito17@gmail.com', 'Organizador', 'Tec. Radiologo', '36246293', NULL, NULL, '2018-11-08 15:17:32', '2018-11-09 20:38:36', '1', 'Efectivo'),
(311, 'Gonzalez Griselda Mabel', '29996651', 'Las Garcitas', 'grismabel19@gmail.com', 'Profesional', 'Tec. Radiologo', '3725484552', NULL, NULL, '2018-11-08 17:33:55', '2018-11-12 15:31:43', '1', 'Efectivo'),
(312, 'Amadey Torrent Maria Adela', '45609768', 'Espana', 'Madey58585858@hotmail.com', 'Profesional', 'Tec. Radiologo', '0054669862590', NULL, NULL, '2018-11-08 17:53:43', '2018-11-12 15:42:52', '1', 'Efectivo'),
(314, 'Alvarez mirian giselle', '36611060', 'Resistencia', 'gisealvarezok@gmail.com', 'Estudiante', 'Estudiante', '3624781791', NULL, NULL, '2018-11-08 19:19:25', '2018-11-12 15:46:08', '1', 'Efectivo'),
(315, 'Sacco Ricardo Silvestre', '13023566', 'Buenos Aires', 'ricardosacco1@hotmail.com', 'Profesional', 'Tec. Radiologo', '0234615654752', NULL, NULL, '2018-11-08 19:22:50', '2018-11-20 19:16:19', '1', 'Efectivo'),
(317, 'Quiñones Miguel Carlos', '13300428', 'Loberia. Pcia. Bs. As.', 'carlos_loberia@hotmail.com.ar', 'Socios al dia', 'Licenciado en Radiologia', '2262493612', NULL, NULL, '2018-11-08 19:29:21', '2018-11-17 11:15:23', '1', 'Efectivo'),
(318, 'SOSA PEDRO', '13572382', 'CORRIENTES-CAPITAL', 'pedrososa_2005@yahoo.com.ar', 'Disertante', 'Licenciado en Produccion de Bioimagenes', '3794001838', NULL, NULL, '2018-11-08 19:52:20', '2018-11-16 19:08:55', '1', 'Efectivo'),
(319, 'Gómez Norma Estefania', '37393303', 'Corrientes', 'Normaestefania21@gmail.com', 'Disertante', 'Técnico superior en diagnóstico por imagen', '3794790956', NULL, NULL, '2018-11-08 20:20:53', '2018-11-08 21:00:05', '1', 'Efectivo'),
(320, 'lazcano ricardo leon', '17259754', 'paso los libres corrientes', 'ricardolaz@hotmail.com', 'Profesional', 'Tecnico Radiologo', '3772561619', NULL, NULL, '2018-11-08 20:32:36', '2018-11-17 11:14:32', '1', 'Efectivo'),
(321, 'hoyos Velardez Jose', '16452686', 'Salta -capital', 'josehj016@gmail.com', 'Profesional', 'Licenciado en Produccion de Bioimagenes', '3874577830', NULL, NULL, '2018-11-08 20:56:22', '2018-11-17 11:12:43', '1', 'Efectivo'),
(322, 'ALARCON RAMON EDUARDO', '20125285', 'Salta -capital', 'realarcon2010@hotmail.com', 'Socios al dia', 'Tecnico Radiologo', '3874460039', NULL, NULL, '2018-11-08 20:58:12', '2018-11-17 11:11:03', '1', 'Efectivo'),
(323, 'Tovar Carlos', '14305151', 'Rosario', 'info@cptr.org.ar', 'Organizador', 'Licenciado en tecnologías Medicas', '3415612103', NULL, NULL, '2018-11-08 21:11:30', '2018-11-17 11:08:18', '1', 'Efectivo'),
(324, 'Mondillo Juan Carlos', '8210271', 'VIEDMA', 'jcmondillo@hotmail.com', 'Profesional', 'Licenciado en Produccion de Bioimagenes', '0292015537609', NULL, NULL, '2018-11-08 21:14:56', '2018-11-20 19:17:12', '1', 'Efectivo'),
(325, 'Flores Edgar', '14525870', 'Rosario', 'info@cptr.org.ar', 'Profesional', 'Licenciado en Produccion de Bioimagenes', '03413998061', NULL, NULL, '2018-11-08 21:17:21', '2018-11-17 11:05:14', '1', 'Efectivo'),
(326, 'Linares Liliana', '11753621', 'Rosario', 'info@cptr.org.ar', 'Profesional', 'Licenciado en tecnologías Medicas', '03413998060', NULL, NULL, '2018-11-08 21:19:52', '2018-11-17 11:02:53', '1', 'Efectivo'),
(327, 'Mauro Ivana', '31767062', 'Rosario', 'info@cptr.org.ar', 'Organizador', 'Tecnico Radiologo', '3416136696', NULL, NULL, '2018-11-08 21:23:08', '2018-11-09 13:26:16', '1', 'Efectivo'),
(328, 'Aranda Juan Nicolas', '11140281', 'LA Rioja', 'juanarandarx@yahoo.com', 'Profesional', 'Licenciado en Produccion de Bioimagenes', '3804679689', NULL, NULL, '2018-11-08 21:27:01', '2018-11-12 15:24:58', '1', 'Efectivo'),
(329, 'Brizuela Rita Maribel', '35496498', 'Resistencia', 'rmbrizuela38@gmail.com', 'Profesional', 'Tec. Radiologo', '3624896257', NULL, NULL, '2018-11-09 12:14:13', '2018-11-17 10:56:24', '1', 'Efectivo'),
(330, 'Ferreyra Maria Laura', '34352965', 'Resistencia', 'maria.lau.ferreyra@gmail.com', 'Organizador', 'Tec. Radiologo', '3794142927', NULL, NULL, '2018-11-09 12:23:40', '2018-11-14 15:12:09', '1', 'Efectivo'),
(331, 'Vukujevic Oraldo Fernando', '18487558', 'Paraguay', 'oraldov@gmail.com', 'Disertante', 'Licenciado en Radiologia', '595981807382', NULL, NULL, '2018-11-09 13:10:05', '2018-11-16 20:42:16', '1', 'Efectivo'),
(333, 'Savarecio Hugo Nestor', '11168982', 'Rosario', 'hnsava7@arnet.com.ar', 'Disertante', 'Tec. Radiologo', '341156954110', NULL, NULL, '2018-11-09 13:29:02', '2018-11-16 20:08:43', '1', 'Efectivo'),
(334, 'Magliolini Ruth Magdalena', '28961522', 'Rosario', 'maglioliniruth@hotmail.com', 'Socios al dia', 'Tecnico Diagnostico por Imagenes', '3435111405', NULL, NULL, '2018-11-09 13:49:18', '2018-11-09 13:50:58', '1', 'Efectivo'),
(335, 'Ojeda Laura Elisabeth', '31872361', 'Resistencia', 'lau_o_85@hotmail.com', 'Profesional', 'Tec. Radiologo', '3624304517', NULL, NULL, '2018-11-09 17:34:26', '2018-11-17 10:55:34', '1', 'Efectivo'),
(336, 'VEGA STELLA MARIS', '5388102', 'LA PALATA', 'smvega_2000@yahoo.com.ar', 'Profesional', 'Tecnica Radiologa', '2215235241', NULL, NULL, '2018-11-09 17:46:00', '2018-11-20 19:13:46', '1', 'Efectivo'),
(337, 'Ledesma Valeria Vanesa', '31511988', 'Saenz Peña', 'valeriavledesma32@gmail.com', 'Profesional', 'Tec. Radiologo', '3625223022', NULL, NULL, '2018-11-09 17:50:51', '2018-11-17 10:54:59', '1', 'Efectivo'),
(338, 'Videla Nancy Elizabeth', '16725914', 'VIEDMA', 'nancivid@hotmail.com', 'Profesional', 'licenciada en Imagenes Medicas', '2920486052', NULL, NULL, '2018-11-09 17:51:12', '2018-11-20 19:14:43', '1', 'Efectivo'),
(339, 'Saucedo Susana', '32350509', 'Corrientes_Capital', 'susisau2@hotmail.es', 'Disertante', 'Tecnica Radiologa', '3794600634', NULL, NULL, '2018-11-09 17:53:25', '2018-11-09 17:57:29', '1', 'Efectivo'),
(340, 'Knoll Mariana', '34036930', 'Saenz Peña Chaco', 'mariana_knoll@utlook.com', 'Disertante', 'Tecnica Radiologa', '3731510746', NULL, NULL, '2018-11-09 17:55:45', '2018-11-16 19:02:59', '1', 'Efectivo'),
(341, 'Rodriguez Zarate Yamila Lorena', '5057065', 'Asuncion- Ita', 'yloreya.28@gmail.com', 'Disertante', 'Licenciada en Radiologia e Imagenologia', '0972166915', NULL, NULL, '2018-11-09 18:10:13', '2018-11-16 20:09:05', '1', 'Efectivo'),
(342, 'Lledo Cynthia Anahi', '2842192', 'Paraguay', 'cynlledo@gmail.com', 'Profesional', 'Licenciada kinesiologa', '0982152433', NULL, NULL, '2018-11-09 18:11:54', '2018-11-17 10:54:18', '1', 'Efectivo'),
(343, 'Cabezas Andres', '167695175', 'Santiago de Chile', 'andres.cabezas@ucentral.cl', 'Disertante', 'Tecnologo Medico', '+56998816534', NULL, NULL, '2018-11-09 18:13:56', '2018-11-16 18:50:27', '1', 'Efectivo'),
(344, 'Nieto Claudio', '17258059', 'Rosario', 'clnieto@hotmail.com', 'Disertante', 'licenciado en Tecnologias Medicas', '0341155617862', NULL, NULL, '2018-11-09 18:16:30', '2018-11-16 19:07:12', '1', 'Efectivo'),
(345, 'Belizan Adian', '28066906', 'Santiago del Estero- Capital', 'abelizan90@gmail.com', 'Disertante', 'Licenciado en Produccion en Bioimagenes', '3854100988', NULL, NULL, '2018-11-09 18:30:39', '2018-11-17 11:27:52', '1', 'Efectivo'),
(346, 'Duarte alicia Adriana', '13962704', 'Cordoba- Capital', 'aldriduarte@hotmail.com', 'Socios al dia', 'Licenciado en Produccion en Bioimagenes', '3516622944', NULL, NULL, '2018-11-09 19:13:18', '2018-11-09 19:22:20', '1', 'Efectivo'),
(347, 'Corrales Ines', '30213616', 'ituziango -Corrientes', 'titacorrales83@gmail.com', 'Organizador', 'Tecnica Radiologa', '03786494645', NULL, NULL, '2018-11-09 19:37:31', '2018-11-28 16:32:19', '1', 'Efectivo'),
(348, 'Fiorucci Gabriela Ines', '21422393', 'Rosario', 'gabrielafiorucci@yahoo.com.ar', 'Socios al dia', 'Tecnica Radiologa', '0341156880075', NULL, NULL, '2018-11-09 19:39:37', '2018-11-09 19:40:11', '1', 'Efectivo'),
(349, 'Portoman Jose Luis', '16371330', 'Rosario', 'jlportoman2207@hotmail.com', 'Disertante', 'Licenciado en Produccion en Bioimagenes', '3415974495', NULL, NULL, '2018-11-09 20:58:44', '2018-11-16 20:40:56', '1', 'Efectivo'),
(350, 'Borda Elisa Karina', '25229012', 'Saenz Peña Chaco', 'karinaborda84@gmail.com', 'Organizador', 'Tecnica Radiologa', '3644650869', NULL, NULL, '2018-11-09 21:02:17', '2018-11-21 03:43:07', '1', 'Efectivo'),
(351, 'PAEZ LEANDRO', '28782727', 'FONTANA', 'sanjavier506@gmail.com', 'Organizador', 'Licenciado en Radiologia', '3624595665', NULL, NULL, '2018-11-09 21:03:47', '2018-11-09 21:04:09', '1', 'Efectivo'),
(352, 'Mora Marcela Carolina', '29220553', 'Corrientes_Capital', 'marcemora35.mm@gimail.com', 'Profesional', 'Tecnica Radiologa', '3794774034', 333914545, NULL, '2018-11-09 21:24:09', '2018-11-09 21:30:40', '1', 'Efectivo'),
(353, 'Asoya Tulio Hector', '21351090', 'Resistencia', 'tuliohasoya@hotmail.com', 'Disertante', 'Tecnico Radiologo', '3624783396', NULL, NULL, '2018-11-09 21:34:52', '2018-11-09 21:37:18', '1', 'Efectivo'),
(354, 'Castellano Migel Angel', '21453436', 'buenos aires', 'migel_castellano2002@yahoo.com.ar', 'Profesional', 'licenciada en Imagenes Medicas', '1156002000', NULL, NULL, '2018-11-09 21:36:02', '2018-11-19 12:40:37', '1', 'Efectivo'),
(355, 'Ludueño Carlos Ariel', '25792194', 'Resistencia', 'arieludueno@hotmail.com', 'Estudiante', 'Estudiante', '3624636298', NULL, NULL, '2018-11-10 12:33:59', '2018-11-12 14:47:07', '1', 'Efectivo'),
(356, 'Almiron Borba Enrique Juan Pablo', '37169454', 'Resistencia', 'borbajuanpablo1993@hotmail.com', 'Estudiante', 'Estudiante', '3624725887', NULL, NULL, '2018-11-10 12:38:23', '2018-11-12 14:47:44', '1', 'Efectivo'),
(357, 'Perez Micaela', '37706945', 'resistencia', 'pmicaela1007@gmail.com', 'Estudiante', 'Estudiante', '3624057268', NULL, NULL, '2018-11-10 12:38:51', '2018-11-12 15:18:52', '1', 'Efectivo'),
(358, 'Bruno Ana Maricel', '24650895', 'Cordoba', 'brunoanam@yahoo.com.ar', 'Disertante', 'Licenciada en Produccion en Bioimagenes', '0351153073071', NULL, NULL, '2018-11-10 13:27:50', '2018-11-16 18:35:16', '1', 'Efectivo'),
(359, 'Romero de los Santos Ruben Abelardo', '43187431', 'España', 'roncoxila@gmail.com', 'Disertante', 'Tecnico Radiologo', '0034664408734', NULL, NULL, '2018-11-10 13:33:24', '2018-11-10 13:34:02', '1', 'Efectivo'),
(360, 'Heredia Sergio', '12365991', 'Cordoba', 'herediaso@hotmail.com', 'Profesional', 'Licenciado en Produccion de Bioimagenes', '3516838388', NULL, NULL, '2018-11-10 13:39:43', '2018-11-20 18:49:33', '1', 'Efectivo'),
(361, 'Silva Juan Claudio', '26696560', 'Resistencia', 'los7herma@hotmail.com', 'Organizador', 'licenciado en Radiologia', '3624256592', NULL, NULL, '2018-11-10 13:42:15', '2018-11-10 13:42:59', '1', 'Efectivo'),
(362, 'Ausilio Fabian Oscar', '20420594', 'Catamarca', 'fabausilio@gmail.com', 'Disertante', 'Licenciado en Produccion de Bioimagenes', '03834474476', NULL, NULL, '2018-11-10 13:45:56', '2018-11-21 18:37:16', '1', 'Efectivo'),
(363, 'Perasso Luis Rodriguez', '24597496', 'Parana, Entre Ríos', 'rodriguezperasso@gmail.com', 'Disertante', 'Licenciado en Produccion de Bioimagenes', '03436231750', NULL, NULL, '2018-11-10 13:50:37', '2018-11-10 13:51:06', '1', 'Efectivo'),
(364, 'Ferrero Carlos Gabriel', '18289311', 'CABA', 'carlosgferrero@gmail.com', 'Disertante', 'Licenciado en Produccion de Bioimagenes', '0111558625363', NULL, NULL, '2018-11-10 13:53:51', '2018-11-18 23:06:09', '1', 'Efectivo'),
(365, 'Robles Franco', '32416586', 'San Miguel', 'roblesdufour@hotmail.com', 'Organizador', 'Tecnico Radiologo', '0362154124207', NULL, NULL, '2018-11-10 13:56:52', '2018-11-10 13:57:17', '1', 'Efectivo'),
(366, 'Girón Imfeld Maira Romina', '33724624', 'Resistencia', 'maira.rgimfeld@gmail.com', 'Organizador', 'Tecnico Radiologo', '0362154878436', NULL, NULL, '2018-11-10 14:00:24', '2018-11-10 14:00:48', '1', 'Efectivo'),
(367, 'Digilio Veronica Alejandra', '22576588', 'Rosario', 'verodigilio@yahoo.com.ar', 'Disertante', 'Tecnico Radiologo', '0341156177181', NULL, NULL, '2018-11-10 14:23:41', '2018-11-16 19:01:25', '1', 'Efectivo'),
(368, 'Victori Agustin Exequiel', '34273005', 'Rosario', 'agustin_1988_TKD@live.com.ar', 'Profesional', 'Tecnico Radiologo', '3415483847', NULL, NULL, '2018-11-10 14:37:57', '2018-11-10 14:48:53', '1', 'Efectivo'),
(369, 'Villalba Maricel Zunilda', '24430778', 'Resistencia', 'markos.zx001@gmail.com', 'Organizador', 'Tecnico Radiologo', '0362154104682', NULL, NULL, '2018-11-10 14:50:33', '2018-11-10 14:50:53', '1', 'Efectivo'),
(370, 'Muraro Fabian', '21879150', 'Cordoba', 'fabianmuraro@hotmail.com', 'Profesional', 'Licenciado en Produccion de Bioimagenes', '03515208308', NULL, NULL, '2018-11-10 14:53:48', '2018-11-20 18:48:12', '1', 'Efectivo'),
(371, 'Rodino Fiorella', '40843032', 'Rosario', 'fiorerodino.97@gmail.com', 'Disertante', 'Tecnico Radiologo', '3413161488', NULL, NULL, '2018-11-10 14:58:19', '2018-11-10 14:58:40', '1', 'Efectivo'),
(372, 'Gonzalez Silvia Liliana', '27440081', 'Resistencia', 'rokami@live.com', 'Organizador', 'Tecnico Radiologo', '362154729320', NULL, NULL, '2018-11-10 15:00:08', '2018-11-10 15:00:26', '1', 'Efectivo'),
(373, 'Vitac Javier', '14943697', 'Comodoro Rivadabia', 'vitajavier@yahoo.com.ar', 'Profesional', 'Tecnico Radiologo', '02974036606', NULL, NULL, '2018-11-10 15:08:42', '2018-11-20 19:14:58', '1', 'Efectivo'),
(374, 'Buceta Daniel Roberto', '12895130', 'Buenos Aires', 'dbuceta@hotmail.com', 'Disertante', 'Licenciado en Produccion de Bioimagenes', '1153236538', NULL, NULL, '2018-11-10 15:10:08', '2018-11-16 18:29:59', '1', 'Efectivo'),
(375, 'Migliaccio Matias', '34400141', 'Buenos Aires', 'matiasm_3@hotmail.com', 'Disertante', 'Tecnico Radiologo', '1168590429', NULL, NULL, '2018-11-10 15:12:41', '2018-12-01 15:29:27', '1', 'Efectivo'),
(376, 'Rudaz Monica', '22524119', 'Resistencia', 'rudazmonica1@gmail.com', 'Organizador', 'Tecnico Radiologo', '3624303063', NULL, NULL, '2018-11-10 17:04:40', '2018-11-20 18:08:24', '1', 'Efectivo'),
(377, 'Oszust Ariel Alejandro', '30392717', 'Resistencia', 'ariel_la88@hotmail.com', 'Organizador', 'Tecnico Radiologo', '3624665853', NULL, NULL, '2018-11-10 17:06:08', '2018-11-10 17:06:35', '1', 'Efectivo'),
(378, 'Retamozo Johana Noemi', '39310595', 'Resistencia', 'ynretamozo@gmail.com', 'Estudiante', 'Estudiante', '3624161083', NULL, NULL, '2018-11-10 17:32:23', '2018-11-12 15:19:25', '1', 'Efectivo'),
(379, 'Halupa Polasek Alejandra', '41516823', 'Resistencia', 'halupaalejandra@gmail.com', 'Estudiante', 'Estudiante', '3624559103', NULL, NULL, '2018-11-10 17:33:43', '2018-11-14 12:23:54', '1', 'Efectivo'),
(380, 'Diaz Lisa Antonella', '40606887', 'Fontana', 'diazlisa01@gmail.com', 'Estudiante', 'Estudiante', '3625233176', NULL, NULL, '2018-11-10 17:36:00', '2018-11-14 12:22:36', '1', 'Efectivo'),
(381, 'Duarte Fernando', '34789913', 'Resistencia', 'rx.coraje@hotmail.com', 'Profesional', 'Tecnico Radiologo', '3624970452', NULL, NULL, '2018-11-10 18:58:02', '2018-11-12 15:36:07', '1', 'Efectivo'),
(382, 'Obregon Jessica Daina', '39314648', 'Barranqueras', 'jes_dai_1996@hotmail.com', 'Estudiante', 'Tecnico Radiologa', '3624109009', NULL, NULL, '2018-11-10 19:01:18', '2018-11-12 15:22:07', '1', 'Efectivo'),
(383, 'Oyuela Jorge Enrique Eduardo', '17666072', 'LA PLATA', 'JOyuela@srt.gob.ar', 'Disertante', 'Tecnica Radiologo', '0221154283548', NULL, NULL, '2018-11-11 00:05:50', '2018-11-16 18:00:48', '1', 'Efectivo'),
(384, 'Furlan Mariano', '23615504', 'La Plata', 'MFurian@srt.gob.ar', 'Disertante', 'Técnico Radiologo', '0221155663113', NULL, NULL, '2018-11-11 02:59:43', '2018-11-11 03:04:51', '1', 'Efectivo'),
(385, 'Lopez Fabiola Vanesa', '28372928', 'Resistencia', 'fay.lopez@yahoo.con', 'Profesional', 'Licenciado en Radiologia e imagen', '362453754', NULL, NULL, '2018-11-16 15:05:55', '2018-11-16 15:06:52', '1', 'Efectivo'),
(386, 'Torrez Magdalena Beatriz', '24727954', 'Resistencia', 'sandy_goitia@hotmail.com', 'Organizador', 'Tec. Radiologo', '3624813924', NULL, NULL, '2018-11-16 16:28:46', '2018-11-20 12:43:09', '1', 'Efectivo'),
(387, 'Agüero Eduardo Josè Antonio', '17008713', 'Fontana', 'eaguero3108@gmail.com', 'Disertante', 'Locenciado en Diagnostico por Imagenes', '3625930562', NULL, NULL, '2018-11-16 22:05:48', '2018-11-16 22:08:38', '1', 'Efectivo'),
(388, 'Fernàndez Marìa Luz', '31709457', 'Buenos Aires', 'luzfernandez@mksargentina.com', 'Disertante', 'Licenciada Física Médica', '3624593997', NULL, NULL, '2018-11-17 03:42:05', '2018-11-17 03:48:27', '1', 'Efectivo'),
(389, 'Encina Rosario Belén', '39177120', 'Resistencia', 'rosario.encina67@gmail.com', 'Organizador', 'Auxiliar en Estimulación Temprana', '3624617385', NULL, NULL, '2018-11-20 23:56:37', '2018-11-21 00:01:33', '1', 'Efectivo'),
(390, 'Torres Griselda Itati', '23742819', 'Corrientes', 'morocha213@hotmail.com', 'Profesional', 'Tec. Radiologo', '3794236618', NULL, NULL, '2018-11-20 23:59:57', '2018-11-21 00:01:06', '1', 'Efectivo'),
(391, 'Ramirez Romina Angélica Itatií', '29395231', 'Corrientes', 'corrientespora2015@gmail.com', 'Profesional', 'Tec. Radiologo', '3794203989', NULL, NULL, '2018-11-21 00:39:59', '2018-11-21 00:40:47', '1', 'Efectivo'),
(392, 'Sosa Vanesa Lorena', '31109708', 'Resistencia', 'vanesasosa2002484@hotmail.com', 'Organizador', 'Tec. Radiologo', '3624788445', NULL, NULL, '2018-11-27 22:33:39', '2018-11-27 22:35:30', '1', 'Efectivo'),
(393, 'Rebora Fraga Carlos Martín', '29657564', 'Resistencia', 'rebora2003@hotmail.com', 'Profesional', 'Tec. Radiologo', '3624603550', NULL, NULL, '2018-11-30 22:23:57', '2018-11-30 22:27:53', '1', 'Efectivo'),
(396, 'Paez leandro Javier', '28782727', 'Fontana', 'sanjavier506@gmail.com', 'Organizador', 'licenciado en Radiología e Imagen', '3624595665', NULL, 1, '2018-12-04 15:46:06', '2018-12-21 13:03:10', '1', 'Efectivo'),
(398, 'Acevedo Marina', '28315724', 'Pcia Roque Saenz Peña', 'acevedomarina93@gmail.com', 'Profesional', 'Técnico Radiologo', '3644229001', NULL, 1, '2018-12-05 17:54:20', '2018-12-21 13:16:31', '0', 'Efectivo'),
(399, 'Torrez Juan Manuel', '31676303', 'Pcia. Roque Sáenz Peña', 'juanmatou@gmail.com', 'Profesional', 'Técnico Radiologo', '03644636447', NULL, 1, '2018-12-05 21:49:23', '2018-12-05 21:49:23', NULL, '1'),
(400, 'Redel Claudio', '24103115', 'Pcia Roque Saenz Peña', 'acevedomarina93@gmail.com', 'Profesional', 'Técnico Radiologo', '3644229001', NULL, 1, '2018-12-06 01:49:27', '2018-12-06 01:49:27', NULL, '1'),
(401, 'Acevedo Marina', '28315724', 'Pcia Roque Saenz Peña', 'acevedomarina93@gmail.com', 'Profesional', 'Técnico Radiologo', '3644229001', NULL, 1, '2018-12-06 01:50:08', '2018-12-06 01:50:08', NULL, '1'),
(402, 'Acevedo Marina', '28315724', 'Pcia Roque Saenz Peña', 'acevedomarina93@gmail.com', 'Profesional', 'Técnico Radiologo', '3644229001', NULL, 1, '2018-12-06 01:50:13', '2018-12-06 01:50:13', NULL, '1'),
(403, 'BORDA ELISA KARINA', '25229012', 'PRESIDENCIA ROQUE SAENZ PEÑA', 'KARINABORDA84@GMAIL.COM', 'Organizador', 'TECNICA SUPERIOR EN RADIOLOGIA', '3644650869', NULL, 1, '2018-12-08 16:48:56', '2018-12-17 17:07:46', '1', 'Efectivo'),
(404, 'MANCUELLO CESAR LUIS', '33147955', 'PRESIDENCIA ROQUE SAENZ PEÑA', 'KARINABORDA84@GMAIL.COM', 'Socios al dia', 'ESTUDIANTE', '3644650869', NULL, 1, '2018-12-08 16:51:53', '2018-12-08 16:51:53', NULL, '1'),
(405, 'CAMPOS TATIANA', '42191498', 'PRESIDENCIA ROQUE SAENZ PEÑA', 'KARINABORDA84@GMAIL.COM', 'Socios al dia', 'ESTUDIANTE', '3644650869', NULL, 1, '2018-12-08 16:53:31', '2018-12-08 16:53:31', NULL, '1'),
(406, 'FRIAS MABEL BEATRIZ', '17465820', 'PRESIDENCIA ROQUE SAENZ PEÑA', 'KARINABORDA84@GMAIL.COM', 'Socios al dia', 'TECNICA SUPERIOR EN RADIOLOGIA', '3644650869', NULL, 1, '2018-12-08 16:54:51', '2018-12-08 16:54:51', NULL, '1'),
(407, 'ALMUA CRISTINA ELISABET', '27377754', 'PRESIDENCIA ROQUE SAENZ PEÑA', 'KARINABORDA84@GMAIL.COM', 'Estudiante', 'ESTUDIANTE', '3644650869', NULL, 1, '2018-12-08 16:57:07', '2018-12-21 13:18:51', '1', 'Efectivo'),
(408, 'SARMIENTO GABRIELA MARINA', '31503188', 'PRESIDENCIA ROQUE SAENZ PEÑA', 'KARINABORDA84@GMAIL.COM', 'Socios al dia', 'ESTUDIANTE', '3644650869', NULL, 1, '2018-12-08 17:19:52', '2018-12-08 17:19:52', NULL, '1'),
(409, 'TOLOZA ROSANA ISABEL', '23321687', 'PRESIDENCIA ROQUE SAENZ PEÑA', 'ROSANAISABELTOLOZA@GMAIL.COM', 'Socios al dia', 'ESTUDIANTE', '3644741950', NULL, 1, '2018-12-08 17:31:52', '2018-12-08 17:31:52', NULL, '1'),
(410, 'MONZON CARLOS WALDEMAR', '29020381', 'PRESIDENCIA ROQUE SAENZ PEÑA', 'KARINABORDA84@GMAIL.COM', 'Socios al dia', 'ESTUDIANTE', '3644273699', NULL, 1, '2018-12-08 17:35:28', '2018-12-08 17:35:28', NULL, '1'),
(411, 'DIAZ SERGIO ALONSO', '28546920', 'PRESIDENCIA ROQUE SAENZ PEÑA', 'SERGIODIAZ_28@HOTMAIL.COM', 'Socios al dia', 'ESTUDIANTE', '3644650869', NULL, 1, '2018-12-08 17:38:47', '2018-12-08 17:38:47', NULL, '1'),
(412, 'BALENZUELA ADRIANA GRACIELA', '30410922', 'PRESIDENCIA ROQUE SAENZ PEÑA', 'GRACEADRIANA@HOTMAIL.COM', 'Socios al dia', 'ESTUDIANTE', '3644650869', NULL, 1, '2018-12-08 17:42:08', '2018-12-08 17:42:08', NULL, '1'),
(413, 'RETAMOZO RITA', '31395940', 'PRESIDENCIA ROQUE SAENZ PEÑA', 'RETAMOZORITA@HOTMAIL.COM', 'Socios al dia', 'ESTUDIANTE', '3644650869', NULL, 1, '2018-12-09 15:58:23', '2018-12-21 13:17:37', '0', 'Efectivo'),
(414, 'Yabbur juan jose', '29968073', 'Juan jose castelli', 'Juanjoyaturco@hotmail.com', 'Organizador', 'Tecnico radiologo', '3644384429', NULL, 1, '2018-12-12 22:18:01', '2018-12-17 17:05:58', '1', 'Efectivo'),
(415, 'Riquel Humberto elias', '14294144', 'J.j.castelli chaco', 'Sanatoriolazaravaldez@hotmail.com', 'Profesional', 'Técnico radiologo', '3644353357', NULL, 1, '2018-12-12 22:26:25', '2018-12-21 13:13:17', '1', 'Efectivo'),
(416, 'Lezcano Ana beatriz', '28955256', 'Castelli', 'analez2310@gmail.com', 'Profesional', 'Técnica Radiologa', '3644740965', NULL, 1, '2018-12-13 11:38:29', '2018-12-21 13:12:59', '1', 'Efectivo'),
(417, 'Colla', '31195253', 'Juan José castelli', 'Silvanac18@hotmail.com', 'Profesional', 'Radiologia', '3644521385', NULL, 1, '2018-12-13 13:16:35', '2018-12-13 13:16:35', NULL, '1'),
(418, 'Colla', '31195253', 'Juan José castelli', 'Silvanac18@hotmail.com', 'Profesional', 'Radiologia', '3644521385', NULL, 1, '2018-12-13 13:17:15', '2018-12-13 13:17:15', NULL, '1'),
(419, 'Silva Juan claudio', '26696560', 'Resistencia', 'Los7herma@Hotmail.com', 'Organizador', 'Lic radiologo', '3624256592', NULL, 1, '2018-12-13 17:49:45', '2018-12-21 13:00:11', '1', 'Efectivo'),
(420, 'Dietz Sebastián', '33144311', 'Catelli', 'Sebastiandietz_87@hotmail.com', 'Profesional', 'Técnico superior en radiología', '03644327477', NULL, 1, '2018-12-14 22:06:45', '2018-12-21 13:02:26', '1', 'Efectivo'),
(421, 'BONAFFINI Ana Maria de los Angeles', '29137728', 'juan jose Castelli', 'eugeniomorienega@hotmail.com', 'Profesional', 'Tecnica Radiologa', '0364154621881', NULL, 1, '2018-12-15 02:08:46', '2018-12-17 17:05:28', '1', 'Efectivo'),
(422, 'COMAN PABLO JAVIER', '33146521', 'PRESIDENCIA ROQUE SAENZ PEÑA', 'KARINABORDA84@GMAIL.COM', 'Estudiante', 'ESTUDIANTE', '3644650869', NULL, 1, '2018-12-15 04:39:28', '2018-12-21 13:16:47', '0', 'Efectivo'),
(423, 'Sanchez Juan Carlos', '23247848', 'Resistencia', 'jcsanchezlic3@gmail.com', 'Organizador', 'Licenciado en Radiologia e imagen', '3624364699', NULL, 1, '2018-12-17 17:02:17', '2018-12-17 17:05:10', '1', 'Efectivo'),
(424, 'Godoy Penayo Natalia Beatriz', '40239086', 'Corrientes', 'natynatss49@gmail.com', 'Estudiante', 'Estudiante', '3794077067', NULL, 1, '2018-12-18 12:21:04', '2018-12-29 15:24:36', '1', 'Efectivo'),
(425, 'Castillo Lelia Beatriz', '40982198', 'Corrientes Capital', 'lelia_cas@hotmail.com', 'Estudiante', 'Estudiante', '3794672515', NULL, 1, '2018-12-18 17:49:54', '2018-12-29 15:25:44', '1', 'Efectivo'),
(426, 'Struciat Oscar', '29019346', 'Basail', 'coas699@hotmail.com', 'Organizador', 'licenciado', '3624593997', NULL, 1, '2018-12-19 19:35:48', '2018-12-21 12:42:46', '1', 'Efectivo'),
(427, 'acevedo monica elisabeth', '30214625', 'fontana', 'monicachaco44@gmail.com', 'Profesional', 'técnica radiologa', '3624223750', NULL, 1, '2018-12-21 13:50:21', '2018-12-21 14:01:47', '1', 'Efectivo'),
(428, 'Gómez Norma Estefanía', '37393303', 'Corrientes', 'Estefy_19_2005@hotmail.com', 'Disertante', 'Disertante', '3794790956', NULL, 1, '2018-12-21 13:56:32', '2018-12-29 15:24:17', '1', 'Efectivo'),
(429, 'Guimenez Guillermo Manuel', '41383092', 'Corrientes capital', 'manuelguimenez8@gmail.com', 'Estudiante', 'Título secundario', '3794776825', NULL, 1, '2018-12-21 14:09:17', '2018-12-29 15:25:31', '1', 'Efectivo'),
(430, 'Cardozo cinthia vanesa', '30571858', 'Fontana  chaco', 'magalicin18086715@gmail.com', 'Disertante', 'Tecnica radiologa', '3624226942', 333914545, 1, '2018-12-21 14:09:51', '2018-12-29 13:33:01', '1', 'Efectivo'),
(431, 'Ivars Ana Ofelia', '16567789', 'Barranqueras', 'dra_anaoivars@hotmail.com', 'Disertante', 'Odontologa', '0362154545678', NULL, 1, '2018-12-21 15:00:56', '2018-12-29 13:35:52', '1', 'Efectivo'),
(432, 'González Jorgelina María soledad', '34540014', 'Corrientes', 'soledadla27ximewal@gmail.com', 'Estudiante', 'Técnico en diagnóstico por imágenes', '3795185546', NULL, 1, '2018-12-21 16:39:07', '2018-12-29 15:25:07', '1', 'Efectivo'),
(433, 'Kazmer Mirna Mabel', '20927164', 'Resistencia', 'Mikaz5@yahoo.com.ar', 'Disertante', 'Odontologa', '3624707375', NULL, 1, '2018-12-21 18:32:58', '2018-12-29 13:32:14', '1', 'Efectivo'),
(434, 'Kazmer Mirna Mabel', '20927164', 'Resistencia', 'Mikaz5@yahoo.com.ar', 'Disertante', 'Odontologa', '3624707375', NULL, 1, '2018-12-21 18:36:35', '2018-12-29 13:31:48', '1', 'Efectivo'),
(435, 'Aquino Natalia', '31458230', 'Resistencia', 'azerbaiyan29@hotmail.com', 'Disertante', 'Mediva veterinaria', '+543624883131', NULL, 1, '2018-12-30 23:31:25', '2019-01-03 09:10:25', '1', 'Efectivo'),
(436, 'GASSMAN MICAELA DAIANA', '34617354', 'CASTELLI', 'micaelarx24@hotmail.com', 'Disertante', 'Tec. Radiologo', '3644341444', NULL, 1, '2019-01-16 19:32:28', '2019-01-16 19:38:47', '1', 'Efectivo'),
(437, 'MORENO CRISTINA NOELIA', '31195339', 'CASTELLI', 'cristinanoeliamoreno84@yahoo.com.ar', 'Disertante', 'Tec. Radiologo', '3644335393', NULL, 1, '2019-01-16 19:36:34', '2019-01-16 19:38:25', '1', 'Efectivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_07_11_141807_inscriptos_table', 1),
(4, '2018_07_12_143435_create_cursos_table', 2),
(5, '2018_07_13_142453_alter_table_inscriptos', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Pablo', 'stella.pablo@gmail.com', '$2y$10$wpktUfgxRqDSp5kKQaO/1.VbofNwxT2xD1VyuBhYXqog8YOPnTN82', '68vW2oCwAOCDhRmZ30FAiaTyUb60DnJpOtexUMQaBBPGb7uxprzDUZOGsyX6', '2018-07-11 21:48:52', '2018-07-11 21:48:52'),
(2, 'Administrador', 'admin@admin.com', '$2y$10$1i/yEle7o.FXx1vhDzh03Oic1zwZo4SWdEkzRJjn3Cvja0z9WRsg6', 'tVveg7ChxkzF4fn5RI9zLtLgwwBEYuZPPwsbP9smzhWG3EcVySSR8EXm99k1', '2018-07-13 15:07:50', '2018-07-13 15:07:50');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inscriptos`
--
ALTER TABLE `inscriptos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inscriptos_email_index` (`email`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `inscriptos`
--
ALTER TABLE `inscriptos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=438;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
