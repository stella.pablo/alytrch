<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
                <!-- /input-group -->
            </li>
            <li class="active">
                <a href="#" class="waves-effect ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="hide-menu"> Inscripciones
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li> <a href="{{ url('inscriptos') }}"> Listado</a> </li>
                    <li> <a href="{{ url('listado') }}"> Emitir inscriptos </a> </li>
                </ul>
            </li>
            <li>
                <a href="#" class="waves-effect">
                    <i class="fa fa-tags"></i>
                    <span class="hide-menu"> Cursos
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li> <a href="{{ url('cursos/create') }}"> Nuevo</a> </li>
                    <li> <a href="{{ url('cursos-disponibles') }}">Listado</a> </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
