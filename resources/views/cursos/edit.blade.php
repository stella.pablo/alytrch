@extends('layouts.master')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">Cursos</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                            {!! Form::model($curso, array('method' => 'PUT', 'route' => array('cursos.update', $curso->id), 'files'=>'true')) !!}

                            <div class="form-body">
                                <h3 class="box-title">CURSOS</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label class="control-label">Titulo</label>
                                            {!! Form::text('titulo', null, ['class' => 'form-control'])!!}
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Fecha</label>
                                            <div class="input-group">
                                                {!! Form::text('fecha', null, ['class' => 'form-control','id'=>'fecha_documento', 'placeholder' => 'dd/mm/yyyy'])!!}
                                                <span class="input-group-addon"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Cupo disponible</label>
                                                <input type="text" name="cupo" value="{{ $curso->cupo }}" class="col-md-3 form-control">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-10">
                                        <label class="control-label">Descripción</label>
                                        <div>
                                            <textarea class="form-control" name="descripcion" rows="5">{{ $curso->descripcion }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label">Imagen:</label>
                                            <div class="input-group">
                                                {!! Form::file('imagen', null, ['class' => 'form-control'])!!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                                </div>
                                <div id="contInputs">
                                </div>


                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        @endsection
        @section('scripts.footer')
            <link href="{{ url('plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')  }}" rel="stylesheet" />
            <script src="{{ url('plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>
            <script>

                $("#fecha_documento").datepicker(
                    { dateFormat: 'yy-mm-dd' }
                );

            </script>

@endsection
