<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="{{ url('invoice/style.css') }}" rel="stylesheet">
    <title>4to Congreso Nacional de Radiología y 11° Encuentro Nacional de Estudiantes<</title>
</head>
<body>
<div id="details" class="clearfix">
    <div id="invoice">
        <div style="text-align: left">
            <img width="92%" src="{{ url('plugins/images/encabezado.png')  }}" />
        </div>
    </div>
</div>

<div>
    <h1 style="margin-left: 10%">CERTIFICADO 5TO CONGRESO NACIONAL DE RADIOLOGIA</h1>

    <div style="margin-left: 3%">
        <p style="margin-bottom: 4%; font-size: 1.1em;">Se certifica que el Sr/a <strong>{{ strtoupper($inscripto->apellido) }}, {{ strtoupper($inscripto->nya) }}</strong>  DNI: <strong>{{ $inscripto->dni }}</strong> ha asistido al<strong> 5TO CONGRESO NACIONAL DE RADIOLOGIA </strong>. Aniversario</p>
        <p style="margin-bottom: 4%; font-size: 1.1em;">del Descubrimiento de los Rayos X, &nbsp; <strong>DE LA ASOCIACIÓN DE LICENCIADOS Y TÉCNICOS EN RADIOLOGÍA DE LA PROVINCIA DEL CHACO”</strong>
        <p style="margin-bottom: 4%; font-size: 1.1em;">en carácter de <strong>{{ strtoupper($inscripto->caracter) }} </strong> con una carga horaria de 15 horas reloj/22,5 horas cátedras-didácticas; Declarada de Interés Legislativo Provincial por</p>
        <p style="margin-bottom: 4%; font-size: 1.1em">Resolución Nº 1623/19 de la Cámara de Diputados de la Provincia del Chaco, Auspiciado por el Ministerio de Salud Pública Provincial, por Resolución  </p>
        <p style="margin-bottom: 4%; font-size: 1.1em; text-align: justify">N° 2850/19. Declarado de Interés Provincial por el Gobierno del Chaco, por Decreto N° 2918/19; y Avalado por la Universidad Tecnológica Nacional</p>

        <p style="margin-bottom: 4%; font-size: 1.1em;">Facultad Resistencia por Resolución N° 612/19.- </p>

        <p style="margin-bottom: 4%; font-size: 1em"> </p>

        <p style="margin-bottom: 7%;  margin-left: 60%; font-size: 1em">Resistencia, Chaco, 15 y 16 de Noviembre de 2019</p></p>

        <p></p>


    </div>
</div>
<div>
    <img style="margin-left: 4%" width="75%" src="{{ url('plugins/images/pie.png')  }}" />
</div>
</body>
</html>