<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>COMPROBANTE DE INSCRIPCION</title>
    <link href="{{ url('invoice/style.css') }}" rel="stylesheet">
</head>
<body>
<div id="details" class="clearfix">
    <div id="invoice">
        <div style="text-align: left">
            <img style="margin-left: 600px;" width="10%" src="{{ url('plugins/images/eliteadmin-logo.png')  }}" />
            <p>Se consta que <strong>{{ $inscripto->nya }}</strong> DNI N° <strong>{{ $inscripto->dni}}</strong> 
            se ha inscripto al curso <strong>36 º Congreso Argentino de Licenciados</strong></p>
            <p><strong> Técnicos Radiólogos</strong> con codigo número 233213FSDF</p>
        </div>
        <div style="text-align: right">
        </div>
    </div>
</div>
</body>
</html>