<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="{{ url('invoice/style.css') }}" rel="stylesheet">
</head>
<body>
<div id="details" class="clearfix">
    <div id="invoice">
        <div style="text-align: left">
            <img style="margin-left: 610px;" width="100px" height="70px" src="{{ url('plugins/images/eliteadmin-logo.png')  }}" />
            <h2>36º Congreso Argentino de Licenciados y Técnicos Radiólogos</h2>
            <h3></h3>
        </div>
        <div style="text-align: right">

        </div>
    </div>
</div>
<table border="0" cellspacing="0" cellpadding="0">
    <thead>
    <tr>
        <th>Fecha</th>
        <th>DNI</th>
        <th>Nombre y Apellido</th>
        <th>Ciudad</th>
        <th>Email</th>
        <th>Contacto</th>
    </tr>
    </thead>
    @foreach($inscriptos as $row)
        <tr>
            <td>{{ $row->created_at->format('d/m/y') }}</td>
            <td>{{ $row->dni }}</td>
            <td>{{ $row->nya }}</td>
            <td>{{ $row->ciudad }}</td>
            <td>{{ $row->email }}</td>
            <td>{{ $row->contacto }}</td>
        </tr>
    @endforeach
</table>
<script type="text/php">
    if (isset($pdf)) {
        $x = 250;
        $y = 960;
        $text = "Hoja {PAGE_NUM} de {PAGE_COUNT}";
        $font = null;
        $size = 10;
        $color = array(0,0,0);
        $word_space = 0.0;  //  default
        $char_space = 0.0;  //  default
        $angle = 0.0;   //  default
        $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
    }
</script>
</body>
</html>