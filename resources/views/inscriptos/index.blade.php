@extends('layouts.master')
@section('content')

    <!-- /row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-warning">
                <div class="panel-heading">INSCRIPTOS
                    <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>
                </div>
            </div>
            <div class="white-box">
                <div class="table-responsive">
                    <table id="myTable" class="table table-striped">
                        <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>DNI</th>
                            <th>Nombre y Apellido</th>
                            <th>Ciudad</th>
                            <th>Email</th>
                            <th>MP</th>
                            <th>Certificado</th>
                            <th>Contacto</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($inscriptos as $row)
                            <tr>
                                <td>{{ $row->created_at->format('d/m/y') }}</td>
                                <td>{{ $row->dni }}</td>
                                <td>{{ $row->nya }}</td>
                                <td>{{ $row->ciudad }}</td>
                                <td>{{ $row->email }}</td>
                                <td>{{ $row->ml_id }}</td>
                                <td>@if($row->pago == 1) {{ "Si" }} @endif</td>
                                <td>{{ $row->contacto }}</td>
                                <td>@if($row->pago == 1)
                                        <span class="label label-success m-r-10">Pagado</span>
                                     @else
                                        <span class="label label-warning m-r-10">Debe</span>
                                     @endif
                                </td>
                                <td>
                                    <a href="{{ route('inscriptos.edit',$row->id) }}"><div class="col-sm-6 col-md-4 col-lg-3"><i title="Editar" class="ti-pencil"></i></div></a>
                                    <a onclick="return confirm('Esta seguro que desea eliminar la inscripcion?')" href="{{ route('inscriptos.delete',$row->id) }}">
                                        <div class="col-sm-6 col-md-4 col-lg-3"><i title="Eliminar" class="ti-alert "></i></div>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
@endsection
@section('scripts.footer')
    <script>
        $(document).ready(function () {
            $('#myTable').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "pageLength": 30,
                "info": true,
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": "Filtro: ",
                    "sInfoEmpty": 'No hay registros que mostrar ',
                    "sInfo": 'Mostrando _END_ filas.',
                    "oPaginate": {
                        "sPrevious": "Anterior",
                        "sNext": "Siguiente"
                    }
                }
            });
            $(document).ready(function () {
                var table = $('#example').DataTable({
                    "columnDefs": [
                        {
                            "visible": false
                            , "targets": 2
                        }
                    ]
                    , "order": [[2, 'asc']]
                    , "displayLength": 25
                    , "drawCallback": function (settings) {
                        var api = this.api();
                        var rows = api.rows({
                            page: 'current'
                        }).nodes();
                        var last = null;
                        api.column(2, {
                            page: 'current'
                        }).data().each(function (group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                                last = group;
                            }
                        });
                    }
                });
            });
        });
    </script>

@endsection



