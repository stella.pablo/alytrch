@extends('layouts.master')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">INSCRIPTOS / Editar </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">



                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @include('layouts.flash')
                        {!! Form::model($inscripto, array('method' => 'PUT', 'route' => array('inscriptos.update', $inscripto->id))) !!}
                        <div class="form-body">
                            <h3 class="box-title">INSCRIPTO</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Nombre y Apellido</label>
                                        {!! Form::text('nya', null, ['class' => 'form-control'])!!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">DNI</label>
                                        {!! Form::text('dni', null, ['class' => 'form-control'])!!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Ciudad</label>
                                        {!! Form::text('ciudad', null, ['class' => 'form-control'])!!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        {!! Form::text('email', null, ['class' => 'form-control'])!!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label class="control-label">Titulo</label>
                                        {!! Form::text('titulo', null, ['class' => 'form-control'])!!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Telefono/Celular</label>
                                        {!! Form::text('contacto', null, ['class' => 'form-control'])!!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">En caracter de</label>
                                        {!! Form::select('caracter', $caracter ,  null, ['class' => 'form-control', 'autofocus'])!!}
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Metodo de pago </label>
                                        {!! Form::select('metodo_pago', $metodo ,  null, ['class' => 'form-control', 'autofocus'])!!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Emitir certificado</label>
                                        {!! Form::checkbox('pago',1,$inscripto->pago, ['class' => 'form-control', 'autofocus'])!!}
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i>Guardar</button>
                            </div>
                            <div id="contInputs">
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
@endsection
