@extends('books.master')
@section('content')


    <table width="70%">
        <thead>
            <th>Name</th>
            <th>Category</th>
            <th>Description</th>
            <th></th>
        </thead>
        <tbody>
            @foreach($books as $row)
                <tr>
                    <td>{{ $row->name  }}</td>
                    <td>{{ $row->category  }}</td>
                    <td>{{ $row->description  }}</td>
                    <td></td>
                </tr>
            @endforeach
        </tbody>

    </table>



@endsection
