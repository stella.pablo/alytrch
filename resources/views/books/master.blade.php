<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Administración</title>
</head>
<body>
    <h1>BOOKS</h1>

    <div class="container">

        @yield('content')

    </div>

    <div class="footer">
        @yield('footer')
    </div>
</body>

</html>
