@extends('books.master')
@section('content')


    <form action="{{ route('books.store')}}" name="books" method="POST" >
        {{ csrf_field() }}


        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <label>Name</label>
        <input type="text" name="name">

        <label>Description</label>
        <input type="text" name="description">

        <label>Category</label>
        <input type="text" name="category">

        <button type="submit">Store</button>
    </form>



@endsection
