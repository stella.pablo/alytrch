@extends('home.master')
@section('content')

    <!-- INICIO LISTADO DE CUROSOS -->
    <p></p>
    <div class="container">

        <div class="card">
            <h5 class="card-header">Curso DISPONIBLE</h5>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-3">
                        <!-- IMAGEN DEL CURSO-->
                        <img src="{{ url('template/img/0_20190903_221857.jpg') }}"
                             alt="..." class="img-thumbnail">
                    </div>
                    <div class="col-lg-5">
                        @if (Session::has('success'))
                            <div class="alert alert-success">
                                <strong>{!! Session::get('success') !!}</strong>
                            </div>
                        @endif
                        <p>Ingrese DNI para emitir el CERTIFICADO al curso

                            <form action="{{ route('getcertificado') }}" method="POST" >
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="formGroupExampleInput">DNI</label>
                                    <input id="dni" type="dni" class="form-control" name="dni" value="{{ old('dni') }}"  autofocus>

                                    @if ($errors->has('dni'))
                                        <span class="help-block" >
                                    <strong style="color: #a94442;">{{ $errors->first('dni') }}</strong>
                                </span>
                                    @endif
                                </div>

                        <p align="center"><button type="submit" class="btn btn-primary mb-2">Consultar</button></p>

                        </form>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <p></p>
    <!-- FINAL LISTADO DE CUROSOS -->

@endsection
