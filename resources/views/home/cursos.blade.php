@extends('home.master')
@section('content')

<!-- INICIO LISTADO DE CUROSOS -->
<p></p>
<div class="container">

    <!-- INICIO CURSO HABILITADO -->
    @foreach($cursos as $row)

    <div class="card">
        <h5 class="card-header">Curso DISPONIBLE</h5>
        <div class="card-body">
                <div class="row">
                <div class="col-lg-3">
                    <!-- IMAGEN DEL CURSO-->
                    <img src="{{ url('template/img/'. $row->imagen) }}" alt="..." class="img-thumbnail">
                </div>
                <div class="col-lg-9">
                    <h5 class="card-title">{{ $row->titulo }}</h5>
                    <p class="card-text">
                        {{ $row->descripcion }}
                    </p>
                        <p align="center"><a href="{{ url('emitir-certificado') }}" class="btn btn-primary">EMITIR CERTIFICADO DE ASISTENCIA</a></p>
                </div>
            </div>
        </div>

    </div>
        <br>
    @endforeach
    <!-- FINAL CURSO HABILITADO -->


</div>
<p></p>
<!-- FINAL LISTADO DE CUROSOS -->

@endsection
