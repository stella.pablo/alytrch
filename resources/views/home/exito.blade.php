@extends('home.master')
@section('content')
<!-- INICIO CURSO -->
<p></p>

<div class="container">

    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card">
                <h5 class="card-header">Inscripción finalizada</h5>
                <div class="card-body">
                    <h5 class="card-title">Usted se ha inscripto exitosamente al curso. </h5>
                    <p class="card-text">
                        Para finalizar la inscripción debe abonar la matrícula al curso en la Asociación de Licenciados y Tecnicos Radiologos del Chaco
                    </p>
                    <p align="center">
                        <a href="{{ url('/') }}" class="btn btn-primary">Listo!</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-lg-3"></div>
    </div>

</div>

<!-- FINAL CURSO -->
@endsection
