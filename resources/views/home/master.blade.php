<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

    <LINK REL=StyleSheet HREF="{{ url('template/css/style.css') }}" TYPE="text/css" MEDIA=screen>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://acdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>


    <title>Cursos - ALYTRCH</title>

</head>

<header>
    <nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark">
        <a class="navbar-brand" href="https://alytrch.com/">Cursos y Talleres</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="https://alytrch.com/">
                        <img src="{{ url('template/img/icon-map.png') }}" alt="" width="15px">
                        Ameghino 45, Local 47, Resistencia - Chaco <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="https://alytrch.com/quienes-somos/">
                        <img src="{{ url('template/img/icon-mail.png') }}" alt="" height="15px">
                        contacto@alytrch.com <span class="sr-only">(current)</span>
                    </a>
                </li>
            </ul>
            <a class="nav-link" href="https://alytrch.com/quienes-somos/">
                <img src="{{ url('template/img/icon-facebook.png') }}" alt="" height="20px">
            </a>
            <a class="nav-link" href="https://alytrch.com/quienes-somos/">
                <img src="{{ url('template/img/icon-twitter.png') }}" alt="" height="20px">
            </a>
            <a class="nav-link" href="https://alytrch.com/quienes-somos/">
                <img src="{{ url('template/img/icon-instagram.png') }}" alt="" height="20px">
            </a>
            <a class="nav-link" href="https://alytrch.com/quienes-somos/">
                <img src="{{ url('template/img/icon-google.png') }}" alt="" height="20px">
            </a>
        </div>
    </nav>
</header>

<body>

@yield('content')


</body>
</html>
