@extends('home.master')
@section('content')

    <!-- INICIO CURSO -->
    <br>
    <div class="container">
        <div class="card container">
            <div class="card-header">
                Curso Disponible
            </div>
            <div class="card-body">
                <div class="row">

                    <div class="col-lg-7">

                        <!-- TITULO EL CURSO -->
                        <h5 class="card-title">Capacitacion de Licenciados y Técnicos Radiólogos</h5>

                        <div class="text-center">
                            <!-- IMAGEN DEL CURSO-->
                            <img src="{{ url('template/img/0_20190903_221857.jpg') }}" class="col-md-10 float-" alt="..." width="50%">
                        </div>

                        <p class="card-text text-justify font-bold" style="font-size: 15px; margin-top: 3%">
                            <!-- DESCRIPCIÓN CORTA DEL CURSO-->
                            <strong>
                                Una nueva edición de este evento de actualización tan importante para el sector radiológico se llevará a cabo en la Provincia del Chaco del 15 y 16 de Noviembre de 2019.
                                La sede será el Aula Magna - UTN Facultad Regional Resistencia. Como siempre, habrá interesantes talleres con una diversidad de temas.
                            </strong>

                        <p style="font-weight: bold">
                            ARANCELES (Precios sujetos hasta el 1 de Noviembre)
                            <ul>
                                <li>Profesionales: $900</li>
                                <li>Estudiantes: $600</li>
                                <li>Socios: $400</li>
                                <li>Fiesta: $500 (precio de tarjeta)</li>
                        </ul>
                        </p>

                        <p style="font-weight: bold">
                            <a target="_blank" href="programa_congreso.pdf" >DESCARGAR PROGRAMA</a></br>
                            Datos de CUENTA Bancaria (en el caso de seleccionar la opcion de pago por Transferencia)
                            </br></br>
                            CBU NRO: 31100302-11000022570035</br>
                            CUIL NRO: 30-71506928-4
                        </p>
                    </div>

                    <!-- INICIO FORMULARIO -->

                    <div class="col-lg-5 alert alert-primary">
                        <h4>Inscripción al curso</h4>


                        <form action="{{ route('inscripcion.store') }}" method="POST" >
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="formGroupExampleInput">Apellido y Nombre</label>
                                <input id="email" type="nya" class="form-control" name="nya" value="{{ old('nya') }}"  autofocus>

                                    @if ($errors->has('nya'))
                                        <span class="help-block" >
                                            <strong style="color: #a94442;">{{ $errors->first('nya') }}</strong>
                                        </span>
                                    @endif
                            </div>


                            <div class="form-group">
                                <label for="formGroupExampleInput2">DNI</label>
                                <input id="dni" type="dni" class="form-control" name="dni" value="{{ old('dni') }}"  autofocus>

                                @if ($errors->has('dni'))
                                    <span class="help-block" >
                                        <strong style="color: #a94442;">{{ $errors->first('dni') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput2">Ciudad</label>
                                <input id="ciudad" type="ciudad" class="form-control" name="ciudad" value="{{ old('ciudad') }}"  autofocus>

                                @if ($errors->has('ciudad'))
                                    <span class="help-block" >
                                        <strong style="color: #a94442;">{{ $errors->first('ciudad') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput2">Email</label>
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}"  autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block" >
                                        <strong style="color: #a94442;">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput2">Título</label>
                                {!! Form::text('titulo', null, ['class' => 'form-control'])!!}
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlSelect1">En carácter de</label>
                                {!! Form::select('caracter', $caracter ,  null, ['class' => 'form-control', 'autofocus'])!!}

                                @if ($errors->has('caracter'))
                                    <span class="help-block" >
                                        <strong style="color: #a94442;">{{ $errors->first('caracter') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput2">Teléfono/Celular</label>
                                <input id="contacto" type="text" class="form-control" name="contacto" value="{{ old('contacto') }}"  autofocus>

                                @if ($errors->has('contacto'))
                                    <span class="help-block" >
                                        <strong style="color: #a94442;">{{ $errors->first('contacto') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput2">Forma de pago</label>
                                {!! Form::select('metodo_pago', $metodo_pago ,  null, ['class' => 'form-control', 'autofocus'])!!}

                                @if ($errors->has('metodo_pago'))
                                    <span class="help-block" >
                                        <strong style="color: #a94442;">{{ $errors->first('metodo_pago') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="check" id="check">
                                <label class="form-check-label" for="exampleCheck1"><strong>Adquirir Tarjeta (Fiesta Fin de año)</strong></label>
                            </div>

                        <hr>
                            <p align="center"><button type="submit" class="btn btn-primary mb-2">Inscribirse</button></p>

                        {{ Form::close() }}
                    </div>
                    <!-- FINAL FORMULARIO -->
                </div>
            </div>
        </div>
    </div>

    <br>

    <!-- FINAL CURSO -->

@endsection
