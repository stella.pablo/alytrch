@extends('home.master')
@section('content')
<!-- INICIO CURSO -->
<p></p>

<div class="container">

    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card">
                <h5 class="card-header">Inscripción</h5>
                <div class="card-body">
                    <h5 class="card-title">DNI no registrado  </h5>
                    <p class="card-text">
                        Para emitir el comprobante debe preinscribirse primero
                    </p>
                </div>
            </div>
        </div>
        <div class="col-lg-3"></div>
    </div>

</div>

<!-- FINAL CURSO -->
@endsection
