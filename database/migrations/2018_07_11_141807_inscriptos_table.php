<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InscriptosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscriptos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nya');
            $table->string('dni');
            $table->string('ciudad');
            $table->string('email')->index();
            $table->string('caracter');
            $table->string('contacto');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscriptos');

    }
}
